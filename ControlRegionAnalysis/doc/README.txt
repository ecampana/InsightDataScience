# Necessary packages that need to be checked out before you can create
# the different control plots

# Must checkout MultLepFit package inside the ControlRegionAnalysis directory
setenv CVSROOT /cms/cmsuser2/trilepton
cvs -d /cms/cmsuser2/trilepton co -d MultLepFit RutgersTools50X/MultLepFit

# Must have the "data" and "FilteredData" directory inside the ControlRegionAnalysis
# directory
ln -s /cms/rcg/rcgray/Statistics/data/* data/.
ln -s /cms/rcg/rcgray/Statistics/FilteredData .

# If you are using the first method to load the macro
  gSystem->Load("pluginAnalysisToolsControlRegionAnalysis.so");

# Then command to run macro is
root -n -l test/run_ControlRegionAnalysis.C+g

# If you are using the second method to load the macro
  gROOT->SetMacroPath("src");
  gROOT->ProcessLine(".L ControlRegionAnalysis.cpp+");

# Then command to run macro is still
root -n -l test/run_ControlRegionAnalysis.C+g

# Note: You may NOT use ClassDef and ClassImp for the ControlRegionAnalysis
# and ChainHistogramAnalysis classes since they seem not to work with
# scramv1 build -j8
