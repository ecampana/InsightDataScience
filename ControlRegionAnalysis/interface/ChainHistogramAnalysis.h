#ifndef ChainHistogramAnalysis_h
#define ChainHistogramAnalysis_h


#include <vector>
#include <string>
#include <sstream>
#include <iostream>

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TFile.h>
#include <TROOT.h>
#include <TString.h>


// Global declaration
typedef std::vector<std::string> vectorOfStrings;

//
// class declaration
//
class ChainHistogramAnalysis {
  // ---------- friend classes and functions ------------------------

  public:
    // ---------- public constants, enums and typedefs --------------

    // ---------- public constructors and destructor ----------------
    ChainHistogramAnalysis();
    virtual ~ChainHistogramAnalysis();

    // ---------- public assignment operator(s) ---------------------

    // ---------- public member functions ---------------------------
    void setCMSLabel( std::string, double );
    void setLuminosity( std::string , double );
    void setStandardPlotStyle( void );

    template<class T> void chainHistograms( std::string, std::string, vectorOfStrings, vectorOfStrings );
    template<class T> void mergeHistograms( std::string, vectorOfStrings, vectorOfStrings );
    template<class T> void mixHistograms( std::string, vectorOfStrings );

    // ---------- public const member functions ---------------------

    // ---------- public static member functions --------------------

    // ---------- public data members -------------------------------
    Double_t xposition;
    Double_t yposition;
    TString cmslabel;
    TString luminosity;

    std::string storefile;

  private:
    // ---------- private constants, enums and typedefs -------------

    // ---------- private constructors and destructor ---------------
    ChainHistogramAnalysis( const ChainHistogramAnalysis & );

    // ---------- private assignment operator(s) --------------------
    const ChainHistogramAnalysis &operator= ( const ChainHistogramAnalysis & );

    // ---------- private member functions --------------------------

    // ---------- private const member functions --------------------

    // ---------- private static member functions -------------------

    // ---------- private data members ------------------------------

  protected:
    // ---------- protected constants, enums and typedefs -----------

    // ---------- protected constructors and destructor -------------

    // ---------- protected assignment operator(s) ------------------

    // ---------- protected member functions ------------------------

    // ---------- protected const member functions ------------------

    // ---------- protected static member functions -----------------

    // ---------- protected data members ----------------------------
};

#endif

#ifndef ChainHistogramAnalysis_cxx
#define ChainHistogramAnalysis_cxx

template<class T> void ChainHistogramAnalysis::chainHistograms(std::string tagname, std::string signame,
                                                               vectorOfStrings histogramnames,
                                                               vectorOfStrings rootfilenames)
{
  std::cout << "In ChainHistogramAnalysis class chainHistograms method!" << std::endl;

  // Reset ROOT
  gROOT->Reset();

  bool debug = false;

  // Set sumw2 on by default and detach histogram from file by default
  TH1::SetDefaultSumw2(kTRUE);
  TH1::AddDirectory(kFALSE); 

  T *temp = new T();
  std::vector<T *> temphistsum;

  // Open output ROOT file
  TFile *g = new TFile(storefile.c_str(), "UPDATE");

  for (int i = 0; i < (int)rootfilenames.size(); i++) {

    if (debug) {
      std::cout << "[EC] Looking for root files: " << rootfilenames[i] << std::endl;
    }

    TFile *f = new TFile(rootfilenames[i].c_str(), "READ");

    for (int j = 0; j < (int)histogramnames.size(); j++) {

      // Get the right names for histograms from the input ROOT files
      std::string histogram = signame + "_" + histogramnames[j];

      if (debug) {
        std::cout << "[EC] Looking for histogram: " << histogram << std::endl;
      }

      temp = (T *)f->Get(histogram.c_str());

      // Decouple temp from file
      temp->SetDirectory(0);

      // Change histogram names to make sure ROOT doesn't read the same histogram again
      std::stringstream ss;
      ss << i;

      std::string namestr = tagname + "_" + histogram + "_" + ss.str();
      temp->SetNameTitle(namestr.c_str(), namestr.c_str());

      // Initialize histogram
      if (i == 0 && j < (int)histogramnames.size()) {
        temphistsum.push_back(temp);
      }

      else {
        temphistsum[j]->Add((T *)temp);
      }

      if (debug) {
        g->cd();
        temp->Write();
      }
    }

    f->Close();
  }

  g->cd();

  for (int i = 0; i < (int)temphistsum.size(); i++) {

    // Name the histograms
    std::string finalname = tagname + "_" + signame + "_" + histogramnames[i];
    temphistsum[i]->SetNameTitle(finalname.c_str(), finalname.c_str());

    // Histograms written to file
    temphistsum[i]->Write();
  }

  // Close output ROOT file
  g->Close();
}

template<class T> void ChainHistogramAnalysis::mergeHistograms(std::string signamecombined,
                                                               vectorOfStrings histogramnames,
                                                               vectorOfStrings mergelist)
{
  std::cout << "In ChainHistogramAnalysis class mergeHistograms method!" << std::endl;

  // Reset ROOT
  gROOT->Reset();

  bool debug = false;

  TFile *f = new TFile(storefile.c_str(), "UPDATE");
  f->cd();

  // Set sumw2 on by default and detach histogram from file by default
  TH1::SetDefaultSumw2(kTRUE);
  TH1::AddDirectory(kFALSE);

  T *temp = new T();
  T *comb = new T();

  std::vector<T *> temphistsum;

  for (int i = 0; i < (int)mergelist.size(); i++) {

    if (debug) {
      std::cout << "[EC] Looking at merge list: " << mergelist[i] << std::endl;
    }

    for (int j = 0; j < (int)histogramnames.size(); j++) {

      // Get the right names for histograms from the input ROOT files
      std::string histogram = mergelist[i] + "_" + histogramnames[j];

      if (debug) {
        std::cout << "[EC] Looking for histogram: " << histogram << std::endl;
      }

      // Decouple temp from file
      temp->SetDirectory(0);

      temp = (T *)f->Get(histogram.c_str());
      temp->SetNameTitle(histogram.c_str(), histogram.c_str());
      comb = (T *)temp->Clone();

      if (i == 0 && j < (int)histogramnames.size()) {
        temphistsum.push_back(comb);
      }

      else {
        temphistsum[j]->Add((T *)comb);
      }
    }
  }

  for (int i = 0; i < (int)temphistsum.size(); i++) {

    // Name the histograms
    std::string finalname = signamecombined + "_" + histogramnames[i];
    temphistsum[i]->SetNameTitle(finalname.c_str(), finalname.c_str());

    // Histograms written to file
    temphistsum[i]->Write();
  }

  f->Close();
}

template<class T> void ChainHistogramAnalysis::mixHistograms(std::string filehistname,
                                                             vectorOfStrings histogramlist)
{
  std::cout << "In ChainHistogramAnalysis class mixHistograms method!" << std::endl;

  // Reset ROOT
  gROOT->Reset();

  bool debug = false;

  TFile *f = new TFile(storefile.c_str(), "UPDATE");
  f->cd();

  // Set sumw2 on by default and detach histogram from file by default
  TH1::SetDefaultSumw2(kTRUE);
  TH1::AddDirectory(kFALSE);

  T *temp = new T();
  T *comb = new T();
  T *temphistsum = new T();

  for (int i = 0; i < (int)histogramlist.size(); i++) {

    if (debug) {
      std::cout << "[EC] Looking for histogram: " << histogramlist[i] << std::endl;
    }

    // Decouple temp from file
    temp->SetDirectory(0);

    temp = (T *)f->Get(histogramlist[i].c_str());
    temp->SetNameTitle(histogramlist[i].c_str(), histogramlist[i].c_str());
    comb = (T *)temp->Clone();

    if (i == 0) {
      temphistsum = comb;
    }

    else {
      temphistsum->Add((T *)comb);
    }
  }

  // Name the histograms
  temphistsum->SetNameTitle(filehistname.c_str(), filehistname.c_str());

  // Histograms written to file
  temphistsum->Write();

  f->Close();
}

#endif
