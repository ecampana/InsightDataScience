#ifndef ControlRegionAnalysis_h
#define ControlRegionAnalysis_h


#include <string>

#include <TString.h>

#include "../plugins/ChainHistogramAnalysis.cpp"


//
// class declaration
//
class ControlRegionAnalysis : public ChainHistogramAnalysis {
  // ---------- friend classes and functions ------------------------

  public:
    // ---------- public constants, enums and typedefs --------------

    // ---------- public constructors and destructor ----------------
    ControlRegionAnalysis();
    virtual ~ControlRegionAnalysis();

    // ---------- public assignment operator(s) ---------------------

    // ---------- public member functions ---------------------------
    void makeTTbarControlPlots( std::vector<std::string>, std::vector<std::string> );
    void makeTTbarControlSTPlots( std::vector<std::string>, std::vector<std::string> );
    void makeWZControlMETPlots( std::vector<std::string>, std::vector<std::string> );
    void makeZZControlPlots( std::vector<std::string>, std::vector<std::string> );
    void makeZGammaControlPlots( std::vector<std::string>, std::vector<std::string> );
    void makeTestControlPlots(std::vector<std::string>, std::vector<std::string> );

    void makeallplots( void );
    void drawBins( std::vector<Int_t>, TString, Int_t, TString );
    TString converttoname( TString );

    // ---------- public const member functions ---------------------

    // ---------- public static member functions --------------------

    // ---------- public data members -------------------------------
    std::vector<Double_t> numParameters;
    std::vector<TString> strParameters;

  private:
    // ---------- private constants, enums and typedefs -------------

    // ---------- private constructors and destructor ---------------
    ControlRegionAnalysis( const ControlRegionAnalysis & );

    // ---------- private assignment operator(s) --------------------
    const ControlRegionAnalysis &operator= ( const ControlRegionAnalysis & );

    // ---------- private member functions --------------------------

    // ---------- private const member functions --------------------

    // ---------- private static member functions -------------------

    // ---------- private data members ------------------------------

  protected:
    // ---------- protected constants, enums and typedefs -----------

    // ---------- protected constructors and destructor -------------

    // ---------- protected assignment operator(s) ------------------

    // ---------- protected member functions ------------------------

    // ---------- protected const member functions ------------------

    // ---------- protected static member functions -----------------

    // ---------- protected data members ----------------------------
};

#endif
