#ifndef ChainHistogramAnalysis_cpp
#define ChainHistogramAnalysis_cpp


#include <iostream>

#include <TStyle.h>

#include "../interface/ChainHistogramAnalysis.h"


//
// constructors and destructor
//

inline ChainHistogramAnalysis::ChainHistogramAnalysis()
{
  std::cout << "In ChainHistogramAnalysis class constructor!" << std::endl;
}

inline ChainHistogramAnalysis::~ChainHistogramAnalysis()
{

}

//
// member functions
//

inline void ChainHistogramAnalysis::setCMSLabel(std::string cms, double ypos)
{
  std::cout << "In ChainHistogramAnalysis class setCMSLabel method! " << std::endl;

  cmslabel = cms;
  yposition = ypos;
}

inline void ChainHistogramAnalysis::setLuminosity(std::string lumi, double xpos)
{
  std::cout << "In ChainHistogramAnalysis class setLuminosity method! " << std::endl;

  luminosity = lumi;
  xposition = xpos;
}

inline void ChainHistogramAnalysis::setStandardPlotStyle(void)
{
  std::cout << "In ChainHistogramAnalysis class setStandardPlotStyle method! " << std::endl;

  TStyle *tdrStyle = new TStyle("tdrStyle","Style for P-TDR");

  // For the canvas:
  tdrStyle->SetCanvasBorderMode(0);
  tdrStyle->SetCanvasColor(kWhite);
  tdrStyle->SetCanvasDefH(600); // Height of canvas
  tdrStyle->SetCanvasDefW(600); // Width of canvas
  tdrStyle->SetCanvasDefX(0);   // X Position on screen
  tdrStyle->SetCanvasDefY(0);   // Y Position on screen

  // For the pad:
  tdrStyle->SetPadBorderMode(0);
  tdrStyle->SetPadColor(kWhite);
  tdrStyle->SetPadGridX(false);
  tdrStyle->SetPadGridY(false);

  // For the grid
  tdrStyle->SetGridColor(0);
  tdrStyle->SetGridStyle(3);
  tdrStyle->SetGridWidth(1);

  // For the frame:
  tdrStyle->SetFrameBorderMode(0);
  tdrStyle->SetFrameBorderSize(1);
  tdrStyle->SetFrameFillColor(0);
  tdrStyle->SetFrameFillStyle(0);
  tdrStyle->SetFrameLineColor(1);
  tdrStyle->SetFrameLineStyle(1);
  tdrStyle->SetFrameLineWidth(1);

  // For the histo:
  tdrStyle->SetHistLineColor(1);
  tdrStyle->SetHistLineStyle(0);
  tdrStyle->SetHistLineWidth(1);
  tdrStyle->SetEndErrorSize(2);
  //tdrStyle->SetErrorX(0.); //THIS BITCH WASTED SEVERAL HOURS BECAUSE IT SET WIDTH OF E2 OPTION TO F***ING 0. WHEN SOMETHING DOESN'T WORK, BREAK THE F***ER CODE..
  tdrStyle->SetMarkerStyle(20);

  // Margins:
  tdrStyle->SetPadLeftMargin(0.13);
  tdrStyle->SetPadRightMargin(0.08);
  tdrStyle->SetPadTopMargin(0.09);
  tdrStyle->SetPadBottomMargin(0.16);

  // For the Global title:
  tdrStyle->SetOptTitle(0);
  tdrStyle->SetTitleFont(42);
  tdrStyle->SetTitleColor(1);
  tdrStyle->SetTitleTextColor(1);
  tdrStyle->SetTitleFillColor(10);
  tdrStyle->SetTitleFontSize(0.05);

  // For the axis titles:
  tdrStyle->SetTitleColor(1, "XYZ");
  tdrStyle->SetTitleFont(42, "XYZ");
  tdrStyle->SetTitleSize(0.06, "XYZ");
  tdrStyle->SetTitleXOffset(0.9);
  tdrStyle->SetTitleYOffset(1.25);

  // For the axis labels:
  tdrStyle->SetLabelColor(1, "XYZ");
  tdrStyle->SetLabelFont(42, "XYZ");
  tdrStyle->SetLabelOffset(0.007, "XYZ");
  tdrStyle->SetLabelSize(0.05, "XYZ");

  // For the axis:
  tdrStyle->SetAxisColor(1, "XYZ");
  tdrStyle->SetStripDecimals(kTRUE);
  tdrStyle->SetTickLength(0.03, "XYZ");
  tdrStyle->SetNdivisions(510, "XYZ");
  tdrStyle->SetPadTickX(1);  // To get tick marks on the opposite side of the frame
  tdrStyle->SetPadTickY(1);

  // Change for log plots:
  tdrStyle->SetOptLogx(0);
  tdrStyle->SetOptLogy(0);
  tdrStyle->SetOptLogz(0);

  // Postscript options:
  tdrStyle->SetPaperSize(20.,20.);

  // For the Option Statistics box
  //tdrStyle->SetOptStat(0);

  // Set Hatches
  tdrStyle->SetHatchesLineWidth(1);

  tdrStyle->cd();
}

#endif
