#ifndef TEMPLATE_cpp
#define TEMPLATE_cpp


#include <iostream>

#include "ANALYSISPATH/interface/TEMPLATE.h"


//ClassImp(TEMPLATE)

//
// constructors and destructor
//

TEMPLATE::TEMPLATE()
{
  std::cout << "In TEMPLATE class constructor!" << std::endl;
}

TEMPLATE::~TEMPLATE()
{

}

//
// member functions
//

#endif
