#ifndef TEMPLATE_h
#define TEMPLATE_h


#include <TObject.h>


//
// class declaration
//
//class TEMPLATE : public TObject {
class TEMPLATE {
  // ---------- friend classes and functions ------------------------

  public:
    // ---------- public constants, enums and typedefs --------------

    // ---------- public constructors and destructor ----------------
    TEMPLATE();
    virtual ~TEMPLATE();

    // ---------- public assignment operator(s) ---------------------

    // ---------- public member functions ---------------------------

    // ---------- public const member functions ---------------------

    // ---------- public static member functions --------------------

    // ---------- public data members -------------------------------

  private:
    // ---------- private constants, enums and typedefs -------------

    // ---------- private constructors and destructor ---------------
    TEMPLATE( const TEMPLATE & );

    // ---------- private assignment operator(s) --------------------
    const TEMPLATE &operator= ( const TEMPLATE & );

    // ---------- private member functions --------------------------

    // ---------- private const member functions --------------------

    // ---------- private static member functions -------------------

    // ---------- private data members ------------------------------

  protected:
    // ---------- protected constants, enums and typedefs -----------

    // ---------- protected constructors and destructor -------------

    // ---------- protected assignment operator(s) ------------------

    // ---------- protected member functions ------------------------

    // ---------- protected const member functions ------------------

    // ---------- protected static member functions -----------------

    // ---------- protected data members ----------------------------

  //ClassDef(TEMPLATE, 1);
};

#endif
