#!/bin/sh

main=`dirname $PWD`
analysispath=`echo ${main} | sed 's/\// /g' | awk '{temp=NF-1; print $temp"\\\/"$NF}'`

newclass=$1
type=$1

mkdir -p ${main}/{interface,src,plugins,test}

cat ${main}/scripts/TEMPLATE.h | sed "s/TEMPLATE/${newclass}/g" > ${main}/interface/${newclass}.h

if [ $2 == "module" ]
then

cat ${main}/scripts/TEMPLATE.cpp | sed "s/TEMPLATE/${newclass}/g" | sed "s/ANALYSISPATH/${analysispath}/g" > ${main}/src/${newclass}.cpp

fi

if [ $2 == "plugin" ]
then

cat ${main}/scripts/TEMPLATE.cpp | sed "s/TEMPLATE/${newclass}/g" | sed "s/ANALYSISPATH/${analysispath}/g" > ${main}/plugins/${newclass}.cpp

fi
