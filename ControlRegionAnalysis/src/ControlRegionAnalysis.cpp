#ifndef ControlRegionAnalysis_cpp
#define ControlRegionAnalysis_cpp

#include <map>
#include <iostream>
#include <fstream>
#include <sstream>

#include <TH1D.h>
#include <TMath.h>
#include <TLine.h>
#include <TLatex.h>
#include <TStyle.h>
#include <Rtypes.h>
#include <THStack.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TGaxis.h>

#include "../interface/ControlRegionAnalysis.h"
#include "../plugins/ChainHistogramAnalysis.cpp"

using namespace std;

//
// constructors and destructor
//

ControlRegionAnalysis::ControlRegionAnalysis()
{
  std::cout << "In ControlRegionAnalysis class constructor!" << std::endl;
}

ControlRegionAnalysis::~ControlRegionAnalysis()
{

}

//
// member functions
//
void ControlRegionAnalysis::makeTTbarControlPlots(std::vector<std::string> dataHist,
                                                  std::vector<std::string> backgroundHist)
{
  std::cout << "In ControlRegionAnalysis class makeTTbarControlPlots method!" << std::endl;

  // Reset ROOT
  gROOT->Reset();

  // Set sumw2 on by default and detach histogram from file by default
  TH1::SetDefaultSumw2(kTRUE);
  TH1::AddDirectory(kFALSE);

  TFile *f = new TFile(storefile.c_str());

  // Converting luminosity string into an integer
  double lumi;
  std::stringstream ss;
  ss << luminosity;
  ss >> lumi;

  TCanvas *c1 = new TCanvas("c1", "c1", 800, 600);
  c1->SetBorderMode(0);
  c1->SetFillColor(kWhite);
  c1->SetLogy();
  c1->cd();

  // Collision data
  TH1D *h_data = new TH1D();
  TH1D *h_ttbar = new TH1D();
  TH1D *h_wz = new TH1D();
  TH1D *h_zz = new TH1D();
  TH1D *h_ttw = new TH1D();
  TH1D *h_ttz = new TH1D();

  // About to get histograms
  if (strParameters[2] == "1D") {
    // Collision data
    h_data = (TH1D *)f->Get(dataHist[0].c_str());

    // TTbar background
    h_ttbar = (TH1D *)f->Get(backgroundHist[0].c_str());

    // WZ background
    h_wz = (TH1D *)f->Get(backgroundHist[1].c_str());

    // ZZ background
    h_zz = (TH1D *)f->Get(backgroundHist[2].c_str());

    // TTW background
    h_ttw = (TH1D *)f->Get(backgroundHist[3].c_str());

    // TTZ background
    h_ttz = (TH1D *)f->Get(backgroundHist[4].c_str());
  }

  if (strParameters[2] == "2D") {
    // Collision data
    h_data = ((TH2D *)f->Get(dataHist[0].c_str()))->ProjectionY();

    // TTbar background
    h_ttbar = ((TH2D *)f->Get(backgroundHist[0].c_str()))->ProjectionY();

    // WZ background
    h_wz = ((TH2D *)f->Get(backgroundHist[1].c_str()))->ProjectionY();

    // ZZ background
    h_zz = ((TH2D *)f->Get(backgroundHist[2].c_str()))->ProjectionY();

    // TTW background
    h_ttw = ((TH2D *)f->Get(backgroundHist[3].c_str()))->ProjectionY();

    // TTZ background
    h_ttz = ((TH2D *)f->Get(backgroundHist[4].c_str()))->ProjectionY();
  }

  // Collision data
  h_data->SetMarkerStyle(8);
  h_data->SetFillColor(kBlack);
  h_data->SetLineColor(kBlack);

  // TTbar background
  h_ttbar->SetFillColor(kMagenta-7);
  h_ttbar->SetLineColor(kBlack);

  // WZ background
  h_wz->SetFillColor(kSpring+7);
  h_wz->SetLineColor(kBlack);

  // ZZ background
  h_zz->SetFillColor(kSpring+4);
  h_zz->SetLineColor(kBlack);

  // TTW background
  h_ttw->SetFillColor(kCyan-3);
  h_ttw->SetLineColor(kBlack);

  // TTZ background
  h_ttz->SetFillColor(kCyan);
  h_ttz->SetLineColor(kBlack);

  // About to get stack
  THStack *hs_background = new THStack("hs_background", "");

  hs_background->Add(h_zz);
  hs_background->Add(h_wz);
  hs_background->Add(h_ttz);
  hs_background->Add(h_ttw);
  hs_background->Add(h_ttbar);

  // Setting maximum y-axis range
  Double_t yaxis_min = 0.05;
  Double_t yaxis_max = 1.0;

  yaxis_max = TMath::Max(h_data->GetMaximum(), hs_background->GetMaximum());

  //hs_background->SetMaximum(1.15*yaxis_max); // For MET plot
  hs_background->SetMaximum(115.0*yaxis_max); // For HT plot
  hs_background->SetMinimum(yaxis_min);
  hs_background->Draw("HIST");

  hs_background->GetYaxis()->SetTitle(strParameters[0]);
  hs_background->GetYaxis()->SetTitleOffset(1.1);
  hs_background->GetYaxis()->SetTitleFont(42);
  hs_background->GetYaxis()->SetTitleSize(0.055);
  hs_background->GetYaxis()->SetLabelFont(42);
  hs_background->GetYaxis()->SetLabelSize(0.05);
  hs_background->GetXaxis()->SetTitle(strParameters[1]);
  hs_background->GetXaxis()->SetTitleOffset(1.2);
  hs_background->GetXaxis()->SetTitleFont(42);
  hs_background->GetXaxis()->SetTitleSize(0.055);
  hs_background->GetXaxis()->SetLabelFont(42);
  hs_background->GetXaxis()->SetLabelSize(0.05);

  //hs_background->GetXaxis()->SetRangeUser(50 ,500); // For MET plot
  hs_background->GetXaxis()->SetRangeUser(100 ,1000); // For HT plot

  // Set MC background systematic by hand
  TH1D *h_systematic_error = new TH1D("","", h_data->GetNbinsX(), h_data->GetXaxis()->GetXmin(), h_data->GetXaxis()->GetXmax());
  h_systematic_error->SetFillColor(kBlue-8);
  h_systematic_error->SetFillStyle(3344);
  h_systematic_error->SetMarkerStyle(0.0);
  
  h_systematic_error->Add(h_zz);
  h_systematic_error->Add(h_wz);
  h_systematic_error->Add(h_ttz);
  h_systematic_error->Add(h_ttw);
  h_systematic_error->Add(h_ttbar);

  // Setting systematic uncertainty
  for (Int_t i = 1; i < (Int_t)h_systematic_error->GetNbinsX()+1; i++) {
    h_systematic_error->SetBinError(i, 0.50*h_systematic_error->GetBinContent(i));
  }

  // About to draw plots
  hs_background->Draw("HIST");
  h_systematic_error->Draw("E2same");
  h_data->Draw("pesame"); // "pex0same"

  // About to draw title
  TLatex *tex = new TLatex();
  tex->SetTextColor(1);
  tex->SetTextAlign(10);
  tex->SetTextSize(0.05);
  tex->SetTextSize(0.040);
  tex->SetTextFont(62); //42
  tex->SetLineWidth(2);
  tex->SetNDC();

  tex->DrawLatex(0.13, yposition, cmslabel);
  //tex->DrawLatex(xposition,0.915, "#sqrt{s} = 8 TeV, L_{int} = "+luminosity+" fb^{-1}");
  //tex->DrawLatex(xposition, 0.915, "#sqrt{s} = 8 TeV, #scale[0.75]{#int}#it{L} d#it{t} = "+luminosity+" fb^{-1}");
  tex->DrawLatex(0.46, 0.940, "#sqrt{s} = 8 TeV");
  tex->DrawLatex(0.775, 0.940, "L = "+luminosity+" fb^{-1}");

  // About to draw legend
  TLegend *leg = new TLegend(0.66, 0.54, 0.89, 0.87, "", "brNDC");
  leg->SetTextFont(42);
  leg->SetFillColor(0);
  leg->SetShadowColor(0);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.03);

  leg->AddEntry(h_data, "Data", "lep");
  leg->AddEntry(h_systematic_error, "Bkg uncertainty", "f");
  leg->AddEntry(h_ttbar, "t#bar{t}", "f");
  leg->AddEntry(h_ttw, "t#bar{t}W", "f");
  leg->AddEntry(h_ttz, "t#bar{t}Z", "f");
  leg->AddEntry(h_wz, "WZ", "f");
  leg->AddEntry(h_zz, "ZZ", "f");

  leg->Draw();
}

void ControlRegionAnalysis::makeTTbarControlSTPlots(std::vector<std::string> dataHist,
                                                    std::vector<std::string> backgroundHist)
{
  std::cout << "In ControlRegionAnalysis class makeTTbarControlPlots method!" << std::endl;

  // Reset ROOT
  gROOT->Reset();

  // Set sumw2 on by default and detach histogram from file by default
  TH1::SetDefaultSumw2(kTRUE);
  TH1::AddDirectory(kFALSE);

  TFile *f = new TFile(storefile.c_str());

  // Converting luminosity string into an integer
  double lumi;
  std::stringstream ss;
  ss << luminosity;
  ss >> lumi;

  TCanvas *c1 = new TCanvas("c1", "c1", 800, 600);
  c1->SetBorderMode(0);
  c1->SetFillColor(kWhite);
  c1->cd();

  TPad *upperPad = new TPad("upperPad", "upperPad", 0.00, 0.215, 1.0, 0.9940476);
  upperPad->Draw();
  upperPad->cd();
  upperPad->SetLogy();
  upperPad->SetBottomMargin(0.135);

  // Collision data
  TH1D *h_data_tmp = new TH1D();
  TH1D *h_ttbar_tmp = new TH1D();
  TH1D *h_wz_tmp = new TH1D();
  TH1D *h_zz_tmp = new TH1D();
  TH1D *h_ttw_tmp = new TH1D();
  TH1D *h_ttz_tmp = new TH1D();

  TH1D *h_data = new TH1D();
  TH1D *h_ttbar = new TH1D();
  TH1D *h_wz = new TH1D();
  TH1D *h_zz = new TH1D();
  TH1D *h_ttw = new TH1D();
  TH1D *h_ttz = new TH1D();

  // About to get histograms
  if (strParameters[2] == "2D") {
    // Collision data
    h_data_tmp = ((TH2D *)f->Get(dataHist[0].c_str()))->ProjectionY();

    // TTbar background
    h_ttbar_tmp = ((TH2D *)f->Get(backgroundHist[0].c_str()))->ProjectionY();

    // WZ background
    h_wz_tmp = ((TH2D *)f->Get(backgroundHist[1].c_str()))->ProjectionY();

    // ZZ background
    h_zz_tmp = ((TH2D *)f->Get(backgroundHist[2].c_str()))->ProjectionY();

    // TTW background
    h_ttw_tmp = ((TH2D *)f->Get(backgroundHist[3].c_str()))->ProjectionY();

    // TTZ background
    h_ttz_tmp = ((TH2D *)f->Get(backgroundHist[4].c_str()))->ProjectionY();
  }

  h_data = new TH1D("h_data", "", h_data_tmp->GetNbinsX(), h_data_tmp->GetXaxis()->GetXmin()/1000.0, h_data_tmp->GetXaxis()->GetXmax()/1000.0);
  h_ttbar = new TH1D("h_data", "", h_data_tmp->GetNbinsX(), h_data_tmp->GetXaxis()->GetXmin()/1000.0, h_data_tmp->GetXaxis()->GetXmax()/1000.0);
  h_wz = new TH1D("h_data", "", h_data_tmp->GetNbinsX(), h_data_tmp->GetXaxis()->GetXmin()/1000.0, h_data_tmp->GetXaxis()->GetXmax()/1000.0);
  h_zz = new TH1D("h_data", "", h_data_tmp->GetNbinsX(), h_data_tmp->GetXaxis()->GetXmin()/1000.0, h_data_tmp->GetXaxis()->GetXmax()/1000.0);
  h_ttw = new TH1D("h_data", "", h_data_tmp->GetNbinsX(), h_data_tmp->GetXaxis()->GetXmin()/1000.0, h_data_tmp->GetXaxis()->GetXmax()/1000.0);
  h_ttz = new TH1D("h_data", "", h_data_tmp->GetNbinsX(), h_data_tmp->GetXaxis()->GetXmin()/1000.0, h_data_tmp->GetXaxis()->GetXmax()/1000.0);

  for (Int_t i = 1; i < (Int_t)h_data_tmp->GetNbinsX()+1; i++) {

    h_data->SetBinContent(i, h_data_tmp->GetBinContent(i));
    h_ttbar->SetBinContent(i, h_ttbar_tmp->GetBinContent(i));
    h_wz->SetBinContent(i, h_wz_tmp->GetBinContent(i));
    h_zz->SetBinContent(i, h_zz_tmp->GetBinContent(i));
    h_ttw->SetBinContent(i, h_ttw_tmp->GetBinContent(i));
    h_ttz->SetBinContent(i, h_ttz_tmp->GetBinContent(i));
  }

  // Collision data
  h_data->SetMarkerStyle(8);
  h_data->SetFillColor(kBlack);
  h_data->SetLineColor(kBlack);

  // TTbar background
  h_ttbar->SetFillColor(kMagenta-7);
  h_ttbar->SetLineColor(kBlack);

  // WZ background
  h_wz->SetFillColor(kSpring+7);
  h_wz->SetLineColor(kBlack);

  // ZZ background
  h_zz->SetFillColor(kSpring+4);
  h_zz->SetLineColor(kBlack);

  // TTW background
  h_ttw->SetFillColor(kCyan-3);
  h_ttw->SetLineColor(kBlack);

  // TTZ background
  h_ttz->SetFillColor(kCyan);
  h_ttz->SetLineColor(kBlack);

  // About to get stack
  THStack *hs_background = new THStack("hs_background", "");

  hs_background->Add(h_zz);
  hs_background->Add(h_wz);
  hs_background->Add(h_ttz);
  hs_background->Add(h_ttw);
  hs_background->Add(h_ttbar);

  // Setting maximum y-axis range
  Double_t yaxis_min = 0.2; // 0.05
  Double_t yaxis_max = 1.0;

  yaxis_max = TMath::Max(h_data->GetMaximum(), hs_background->GetMaximum());

  hs_background->SetMaximum(50.0*yaxis_max); // 1.15
  hs_background->SetMinimum(yaxis_min);
  hs_background->Draw("HIST");

  hs_background->GetYaxis()->SetTitle(strParameters[0]);
  hs_background->GetYaxis()->SetTitleOffset(1.0);
  hs_background->GetYaxis()->SetTitleFont(42);
  hs_background->GetYaxis()->SetTitleSize(0.055); // 0.045
  hs_background->GetYaxis()->SetLabelFont(42);
  hs_background->GetYaxis()->SetLabelSize(0.05); // 0.045
  hs_background->GetXaxis()->SetTitle(strParameters[1]);
  hs_background->GetXaxis()->SetTitleOffset(1.5);
  hs_background->GetXaxis()->SetTitleFont(42);
  hs_background->GetXaxis()->SetTitleSize(0.055); // 0.045
  hs_background->GetXaxis()->SetLabelOffset(1.4);
  hs_background->GetXaxis()->SetLabelFont(42);
  hs_background->GetXaxis()->SetLabelSize(0.05);
  //hs_background->GetXaxis()->SetRangeUser(300, 1590);
  hs_background->GetXaxis()->SetRangeUser(0.300, 1.590);

  // TTbar ST plot
  TH1D *h_systematic_error = new TH1D("","", h_data->GetNbinsX(), h_data->GetXaxis()->GetXmin(), h_data->GetXaxis()->GetXmax());
  h_systematic_error->SetFillColor(kBlue-8);
  h_systematic_error->SetFillStyle(3344);
  h_systematic_error->SetMarkerStyle(0.0);

  h_systematic_error->Add(h_zz);
  h_systematic_error->Add(h_wz);
  h_systematic_error->Add(h_ttz);
  h_systematic_error->Add(h_ttw);
  h_systematic_error->Add(h_ttbar);

  // Setting systematic uncertainty
  for (Int_t i = 1; i < h_systematic_error->GetNbinsX()+1; i++) {
    h_systematic_error->SetBinError(i, 0.50*h_systematic_error->GetBinContent(i));
  }

  setStandardPlotStyle();

  // About to draw plots
  hs_background->Draw("HIST");
  h_systematic_error->Draw("E2same");
  h_data->Draw("pesame"); // "pex0same"

  // About to draw title
  TLatex *tex = new TLatex();
  tex->SetTextColor(1);
  tex->SetTextAlign(10);
  tex->SetTextSize(0.05);
  tex->SetTextSize(0.040);
  tex->SetTextFont(62); //42
  tex->SetLineWidth(2);
  tex->SetNDC();

  tex->DrawLatex(0.13, yposition, cmslabel);
  //tex->DrawLatex(xposition,0.915, "#sqrt{s} = 8 TeV, L_{int} = "+luminosity+" fb^{-1}");
  tex->DrawLatex(xposition, 0.915, "#sqrt{s} = 8 TeV, #scale[0.75]{#int}#it{L} d#it{t} = "+luminosity+" fb^{-1}");

  // About to draw legend  
  TLegend *leg = new TLegend(0.65, 0.59, 0.88, 0.87, "", "brNDC");
  leg->SetTextFont(42);
  leg->SetFillColor(0);
  leg->SetShadowColor(0);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.045);

  leg->AddEntry(h_data, "Data", "lep");
  leg->AddEntry(h_systematic_error, "Bkg uncertainty", "f");
  leg->AddEntry(h_ttbar, "t#bar{t}", "f");
  leg->AddEntry(h_ttw, "t#bar{t}W", "f");
  leg->AddEntry(h_ttz, "t#bar{t}Z", "f");
  leg->AddEntry(h_wz, "WZ", "f");
  leg->AddEntry(h_zz, "ZZ", "f");

  leg->Draw();

  c1->cd();

  TPad *lowerPad = new TPad("lowerPad", "lowerPad", 0.00, 0.008, 1.0, 0.32);
  lowerPad->Draw();
  lowerPad->cd();

  lowerPad->SetTopMargin(0.00); //0.01
  lowerPad->SetBottomMargin(0.3); // 0.24

  TH1D *h_ratio = (TH1D *)h_data->Clone();
  h_ratio->SetMinimum(0.0);
  h_ratio->SetMaximum(2.0);

  h_ratio->GetYaxis()->SetTitle("Data/Bkg");
  h_ratio->GetYaxis()->CenterTitle(1);
  h_ratio->GetYaxis()->SetTitleOffset(0.4);
  h_ratio->GetYaxis()->SetTitleFont(42);
  h_ratio->GetYaxis()->SetTitleSize(0.12);
  h_ratio->GetYaxis()->SetLabelFont(42);
  h_ratio->GetYaxis()->SetLabelSize(0.09);
  h_ratio->GetYaxis()->SetLabelOffset(0.01);
  h_ratio->GetYaxis()->SetNdivisions(505);
  h_ratio->GetYaxis()->SetTickLength(0.01);
  h_ratio->GetXaxis()->SetTitle(strParameters[1]);
  h_ratio->GetXaxis()->SetTitleOffset(1.2);
  h_ratio->GetXaxis()->SetTitleFont(42);
  h_ratio->GetXaxis()->SetTitleSize(0.12);
  h_ratio->GetXaxis()->SetLabelOffset(0.01);
  h_ratio->GetXaxis()->SetLabelFont(42);
  h_ratio->GetXaxis()->SetLabelSize(0.125); // 0.9

  h_ratio->Divide(h_systematic_error);

  //h_ratio->GetXaxis()->SetRangeUser(300, 1590);
  h_ratio->GetXaxis()->SetRangeUser(0.300, 1.590);
  h_ratio->GetYaxis()->SetRangeUser(0.0, 2.2); // -2.2

  gStyle->SetOptStat(0);

  h_ratio->Draw();

  //c1->cd();
  //c1->Range(-10,-1,10,1);

  //TGaxis *axis1 = new TGaxis(-8.0,-0.5,8.0,-0.5,0.4,1.6,25,"");
  //axis1->Draw("same");
}

void ControlRegionAnalysis::makeWZControlMETPlots(std::vector<std::string> dataHist,
                                                  std::vector<std::string> backgroundHist)
{
  std::cout << "In ControlRegionAnalysis class makeWZControlPlots method!" << std::endl;

  // Reset ROOT
  gROOT->Reset();

  // Set sumw2 on by default and detach histogram from file by default
  TH1::SetDefaultSumw2(kTRUE);
  TH1::AddDirectory(kFALSE);

  TFile *f = new TFile(storefile.c_str());

  // Converting luminosity string into an integer
  double lumi;
  std::stringstream ss;
  ss << luminosity;
  ss >> lumi;

  TCanvas *c1 = new TCanvas("c1", "c1", 800, 600);
  c1->SetBorderMode(0);
  c1->SetFillColor(kWhite);
  c1->cd();

  // Collision data
  TH1D *h_data = new TH1D();
  TH1D *h_ttbar = new TH1D();
  TH1D *h_wz = new TH1D();
  TH1D *h_zz = new TH1D();
  TH1D *h_ttw = new TH1D();
  TH1D *h_ttz = new TH1D();
  TH1D *h_ddbackground = new TH1D();

  // MT 50-100 GeV
  Double_t min = 6;
  Double_t max = 10;

  // About to get histograms
  if (strParameters[2] == "2D") {
    // Collision data
    h_data = ((TH2D *)f->Get(dataHist[0].c_str()))->ProjectionX("", min, max);

    // TTbar background
    h_ttbar = ((TH2D *)f->Get(backgroundHist[0].c_str()))->ProjectionX("", min, max);

    // WZ background
    h_wz = ((TH2D *)f->Get(backgroundHist[1].c_str()))->ProjectionX("", min, max);

    // ZZ background
    h_zz = ((TH2D *)f->Get(backgroundHist[2].c_str()))->ProjectionX("", min, max);

    // TTW background
    h_ttw = ((TH2D *)f->Get(backgroundHist[3].c_str()))->ProjectionX("", min, max);

    // TTZ background
    h_ttz = ((TH2D *)f->Get(backgroundHist[4].c_str()))->ProjectionX("", min, max);

    // Data-driven background
    h_ddbackground = ((TH2D *)f->Get(backgroundHist[5].c_str()))->ProjectionX("", min, max);
  }

  // Collision data
  h_data->SetMarkerStyle(8);
  h_data->SetFillColor(kBlack);
  h_data->SetLineColor(kBlack);

  // TTbar background
  h_ttbar->SetFillColor(kMagenta-7);
  h_ttbar->SetLineColor(kBlack);

  // WZ background
  h_wz->SetFillColor(kSpring+7);
  h_wz->SetLineColor(kBlack);

  // ZZ background
  h_zz->SetFillColor(kSpring+4);
  h_zz->SetLineColor(kBlack);

  // TTW background
  h_ttw->SetFillColor(kCyan-3);
  h_ttw->SetLineColor(kBlack);

  // TTZ background
  h_ttz->SetFillColor(kCyan);
  h_ttz->SetLineColor(kBlack);

  // Data-driven background
  h_ddbackground->SetFillColor(kYellow);
  h_ddbackground->SetLineColor(kBlack);

  // About to get stack
  THStack *hs_background = new THStack("hs_background", "");

  hs_background->Add(h_zz);
  hs_background->Add(h_wz);
  hs_background->Add(h_ttz);
  hs_background->Add(h_ttw);
  hs_background->Add(h_ttbar);
  hs_background->Add(h_ddbackground);

  // Setting maximum y-axis range
  Double_t yaxis_min = 0.05;
  Double_t yaxis_max = 1.0;

  yaxis_max = TMath::Max(h_data->GetMaximum(), hs_background->GetMaximum());

  hs_background->SetMaximum(1.15*yaxis_max);
  hs_background->SetMinimum(yaxis_min);
  hs_background->Draw("HIST");

  hs_background->GetYaxis()->SetTitle(strParameters[0]);
  hs_background->GetYaxis()->SetTitleOffset(1.0);
  hs_background->GetYaxis()->SetTitleFont(42);
  hs_background->GetYaxis()->SetTitleSize(0.045);
  hs_background->GetYaxis()->SetLabelFont(42);
  hs_background->GetYaxis()->SetLabelSize(0.035);
  hs_background->GetXaxis()->SetTitle(strParameters[1]);
  hs_background->GetXaxis()->SetTitleOffset(1.1);
  hs_background->GetXaxis()->SetTitleFont(42);
  hs_background->GetXaxis()->SetTitleSize(0.045);
  hs_background->GetXaxis()->SetLabelFont(42);
  hs_background->GetXaxis()->SetLabelSize(0.035);

  TH1D *h_systematic_error = new TH1D("","", 30, 0, 300);
  h_systematic_error->SetFillColor(kBlue-8);
  h_systematic_error->SetFillStyle(3344);
  h_systematic_error->SetMarkerStyle(0.0);

  h_systematic_error->Add(h_zz);
  h_systematic_error->Add(h_wz);
  h_systematic_error->Add(h_ttz);
  h_systematic_error->Add(h_ttw);
  h_systematic_error->Add(h_ttbar);
  h_systematic_error->Add(h_ddbackground);

  // Setting systematic uncertainty
  for (Int_t i = 1; i < 31; i++) {
    h_systematic_error->SetBinError(i, 0.15*h_systematic_error->GetBinContent(i));
  }

  // About to draw plots
  hs_background->Draw("HIST");
  h_systematic_error->Draw("E2same");
  h_data->Draw("pesame"); // "pex0same"

  // About to draw title
  TLatex *tex = new TLatex();
  tex->SetTextColor(1);
  tex->SetTextAlign(10);
  tex->SetTextSize(0.05);
  tex->SetTextSize(0.040);
  tex->SetTextFont(62); //42
  tex->SetLineWidth(2);
  tex->SetNDC();

  tex->DrawLatex(0.13, yposition, cmslabel);
  tex->DrawLatex(xposition,0.915, "#sqrt{s} = 8 TeV, L_{int} = "+luminosity+" fb^{-1}");
  //tex->DrawLatex(xposition, 0.915, "#sqrt{s} = 8 TeV, #scale[0.75]{#int}#it{L} d#it{t} = "+luminosity+" fb^{-1}");

  // About to draw legend  
  TLegend *leg = new TLegend(0.65, 0.29, 0.88, 0.87, "", "brNDC");
  leg->SetTextFont(42);
  leg->SetFillColor(0);
  leg->SetShadowColor(0);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.03);

  leg->AddEntry(h_data, "Data", "lep");
  leg->AddEntry(h_systematic_error, "Bkg uncertainty", "f");
  leg->AddEntry(h_ddbackground, "Data-driven", "f");
  leg->AddEntry(h_ttbar, "t#bar{t}", "f");
  leg->AddEntry(h_ttw, "t#bar{t}W", "f");
  leg->AddEntry(h_ttz, "t#bar{t}Z", "f");
  leg->AddEntry(h_wz, "WZ", "f");
  leg->AddEntry(h_zz, "ZZ", "f");

  leg->Draw();
}

void ControlRegionAnalysis::makeZZControlPlots(std::vector<std::string> dataHist,
                                               std::vector<std::string> backgroundHist)
{
  std::cout << "In ControlRegionAnalysis class makeZZControlPlots method!" << std::endl;

  // Reset ROOT
  gROOT->Reset();

  // Set sumw2 on by default and detach histogram from file by default
  TH1::SetDefaultSumw2(kTRUE);
  TH1::AddDirectory(kFALSE);

  TFile *f = new TFile(storefile.c_str());

  // Converting luminosity string into an integer [fb]
  double lumi;
  std::stringstream ss;
  ss << luminosity;
  ss >> lumi;

  TCanvas *c1 = new TCanvas("c1", "c1", 800, 600);
  c1->SetBorderMode(0);
  c1->SetFillColor(kWhite);
  c1->cd();

  // MET 0-50 GeV
  Double_t min = 0;
  Double_t max = 5;

  // CFO uncertainties
  //Double_t cmu = 0.00714*0.5*(1.0465+0.74);
  Double_t cmu_err = 0.30;

  //Double_t ce = 0.00914*0.5*(0.6919+0.80);
  //Double_t ce_err = 0.30;

  //Double_t tau = 0.20*1.0768; // Low HT
  //Double_t tau = 0.05*0.96; // High HT
  //Double_t tau_err = 0.30;

  // Asymmetric Interanl Photon Conversion uncertainties
  //Double_t convmu = 0.00707;
  //Double_t convmu_err = 0.50;

  //Double_t conve = 0.0198;
  //Double_t conve_err = 0.5;

  // Data luminosity [pb]
  Double_t data_lumi = 1000*lumi;

  // Branching ratios
  Double_t WtoLNu = 0.3203;
  Double_t Wto2J = (1.0-0.3203);

  // Cross sections [pb]
  Double_t xsec_ttbar = 225.0;
  Double_t xsec_ttbar_fullyleptonic = pow(WtoLNu,2)*xsec_ttbar;
  Double_t xsec_ttbar_semileptonic = 2.0*WtoLNu*Wto2J*xsec_ttbar;
  Double_t xsec_wz = 32.3*(0.3257*0.10095)*1.20;
  Double_t xsec_zz = 0.92*17.7*(0.10095*0.10095)*1.08;
  Double_t xsec_ttw = 0.23;
  Double_t xsec_ttz = 0.208;

  // Number of generated events
  Double_t N_ttbar_fullyleptonic = 9.20104e+05+4.53536e+05+4.61283e+05+4.55184e+05+4.62597e+05+4.61627e+05+4.61345e+05+4.59597e+05+3.41874e+05;
  Double_t N_ttbar_semileptonic = 2.40481e+05+8.31427e+05+1.59079e+05+1.61002e+05+1.60029e+05+1.60359e+05+1.5889e+05+1.60191e+05+1.60265e+05+1.10807e+05;
  Double_t N_wz = 2.96073e+05+1.98916e+05+0.000000e+00;
  Double_t N_zz = 6.75305e+05+4.46457e+05+4.41662e+05+1.52382e+05;
  Double_t N_ttw = 2.17827e+05;
  Double_t N_ttz = 2.09677e+05;

  // Multilepton filtering efficiencies
  Double_t ttbar_filter_fullyleptonic = 0.3697;
  Double_t ttbar_filter_semileptonic = 0.064;
  Double_t wz_filter = 0.244;
  Double_t zz_filter = 0.358;

  // Scaling factors
  Double_t alpha_ttbar = (ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic)*data_lumi/(ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic/(ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic)*N_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic*(ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic)*N_ttbar_semileptonic);
  Double_t alpha_wz = wz_filter*xsec_wz*data_lumi/N_wz;
  Double_t alpha_zz = zz_filter*xsec_zz*data_lumi/N_zz;
  Double_t alpha_ttw = xsec_ttw*data_lumi/N_ttw;
  Double_t alpha_ttz = xsec_ttz*data_lumi/N_ttz;

  // Errors
  Double_t max_error = 0.0;
  Double_t total_error = 0.0;
  Double_t total_bin_error = 0.0;

  // Collision data
  TH1D *h_data = new TH1D();
  TH1D *h_ttbar = new TH1D();
  TH1D *h_wz = new TH1D();
  TH1D *h_zz = new TH1D();
  TH1D *h_ttw = new TH1D();
  TH1D *h_ttz = new TH1D();
  TH1D *h_higgs = new TH1D();
  TH1D *h_ddbackground = new TH1D();

  // For Data-driven systematics
  TH1D *h_ddbackground_Err = new TH1D();

  // About to get histograms
  if (strParameters[2] == "2D") {
    // Collision data
    h_data = (TH1D *)((TH2D *)f->Get(dataHist[0].c_str()))->ProjectionX("h_data", min, max)->Rebin(4);

    // TTbar background
    h_ttbar = (TH1D *)((TH2D *)f->Get(backgroundHist[0].c_str()))->ProjectionX("h_ttbar", min, max)->Rebin(4);

    // WZ background
    h_wz = (TH1D *)((TH2D *)f->Get(backgroundHist[1].c_str()))->ProjectionX("h_wz", min, max)->Rebin(4);

    // ZZ background
    h_zz = (TH1D *)((TH2D *)f->Get(backgroundHist[2].c_str()))->ProjectionX("h_zz", min, max)->Rebin(4);

    // TTW background
    h_ttw = (TH1D *)((TH2D *)f->Get(backgroundHist[3].c_str()))->ProjectionX("h_ttw", min, max)->Rebin(4);

    // TTZ background
    h_ttz = (TH1D *)((TH2D *)f->Get(backgroundHist[4].c_str()))->ProjectionX("h_ttz", min, max)->Rebin(4);

    // Data-driven background
    h_ddbackground = (TH1D *)((TH2D *)f->Get(backgroundHist[5].c_str()))->ProjectionX("h_ddbackground", min, max)->Rebin(4);

    // Higgs background
    h_higgs = (TH1D *)((TH2D *)f->Get(backgroundHist[6].c_str()))->ProjectionX("h_higgs", min, max)->Rebin(4);

    // Data-driven h_ddbackground_Err
    h_ddbackground_Err = (TH1D *)h_data->Clone();
    h_ddbackground_Err->Reset("ICES");
  }

  // Collision data
  h_data->SetMarkerStyle(8);
  h_data->SetFillColor(kBlack);
  h_data->SetLineColor(kBlack);

  // TTbar background
  h_ttbar->SetFillColor(kMagenta-7);
  h_ttbar->SetLineColor(kBlack);

  // WZ background
  h_wz->SetFillColor(kSpring+7);
  h_wz->SetLineColor(kBlack);

  // ZZ background
  h_zz->SetFillColor(kSpring+4);
  h_zz->SetLineColor(kBlack);

  // TTW background
  h_ttw->SetFillColor(kCyan-3);
  h_ttw->SetLineColor(kBlack);

  // TTZ background
  h_ttz->SetFillColor(kCyan);
  h_ttz->SetLineColor(kBlack);

  // Higgs background
  h_higgs->SetFillColor(92);
  h_higgs->SetLineColor(kBlack);

  // Data-driven background
  h_ddbackground->SetFillColor(kYellow);
  h_ddbackground->SetLineColor(kBlack);

  // Systematic errors
  TH1D *h_systematic_error = new TH1D("", "", 20, 0, 400);
  h_systematic_error->SetFillColor(kBlue-8);
  h_systematic_error->SetFillStyle(3344);
  h_systematic_error->SetMarkerStyle(0.0);
  
  h_systematic_error->Add(h_zz);
  h_systematic_error->Add(h_wz);
  h_systematic_error->Add(h_ttz);
  h_systematic_error->Add(h_ttw);
  h_systematic_error->Add(h_ttbar);
  h_systematic_error->Add(h_higgs);
  h_systematic_error->Add(h_ddbackground);

  // Setting systematic uncertainty
  for (Int_t i = 1; i < h_systematic_error->GetNbinsX(); i++) {

    h_ddbackground_Err->SetBinContent(i,cmu_err*h_ddbackground->GetBinContent(i));

    total_bin_error = sqrt(pow(h_ddbackground_Err->GetBinContent(i),2)+h_ddbackground->GetBinContent(i)+pow(0.50*h_ttbar->GetBinContent(i),2)+pow(0.06*h_ttbar->GetBinContent(i),2)+pow(alpha_ttbar*sqrt(h_ttbar->GetBinContent(i)),2)+pow(0.06*h_wz->GetBinContent(i),2)+pow(alpha_wz*sqrt(h_wz->GetBinContent(i)),2)+pow(0.12*h_zz->GetBinContent(i),2)+pow(alpha_zz*sqrt(h_zz->GetBinContent(i)),2)+pow(0.5*h_ttw->GetBinContent(i),2)+pow(alpha_ttw*sqrt(h_ttw->GetBinError(i)),2)+pow(0.5*h_ttz->GetBinContent(i),2)+pow(alpha_ttz*sqrt(h_ttz->GetBinContent(i)),2));

    h_systematic_error->SetBinError(i, total_bin_error);

    max_error = TMath::Max(max_error, total_bin_error);
  }

  // Calculating total error
  for (Int_t i = 1; i < h_systematic_error->GetNbinsX(); i++) {
      total_error += h_systematic_error->GetBinError(i);
  }

  // About to get stack
  THStack *hs_background = new THStack("hs_background", "");

  hs_background->Add(h_ddbackground);
  hs_background->Add(h_ttbar);
  hs_background->Add(h_zz);
  hs_background->Add(h_wz);
  hs_background->Add(h_ttz);
  hs_background->Add(h_ttw);
  hs_background->Add(h_higgs);

  // Setting maximum y-axis range
  Double_t yaxis_min = 0.0;
  Double_t yaxis_max = 1.0;

  yaxis_max = TMath::Max(h_data->GetMaximum(), hs_background->GetMaximum()+max_error);

  hs_background->SetMaximum(1.25*yaxis_max);
  hs_background->SetMinimum(yaxis_min);
  hs_background->Draw("HIST");

  hs_background->GetYaxis()->SetTitle(strParameters[0]);
  hs_background->GetYaxis()->SetTitleOffset(1.1);
  hs_background->GetYaxis()->SetTitleFont(42);
  hs_background->GetYaxis()->SetTitleSize(0.055);
  hs_background->GetYaxis()->SetLabelFont(42);
  hs_background->GetYaxis()->SetLabelSize(0.040);
  hs_background->GetXaxis()->SetTitle(strParameters[1]);
  hs_background->GetXaxis()->SetTitleOffset(1.2);
  hs_background->GetXaxis()->SetTitleFont(42);
  hs_background->GetXaxis()->SetTitleSize(0.055);
  hs_background->GetXaxis()->SetLabelFont(42);
  hs_background->GetXaxis()->SetLabelSize(0.040);

  // Print out statements
  std::cout << "Total Observation: "<< h_data->Integral(0, h_data->GetNbinsX()) << std::endl;
  std::cout << "Total Background: " << h_systematic_error->Integral(0, h_systematic_error->GetNbinsX()) << std::endl;
  std::cout << "Total Background Uncertainty: " << total_error << std::endl;
  std::cout << "Total Data-driven: "<< h_ddbackground->Integral(0, h_ddbackground->GetNbinsX()) << std::endl;
  std::cout << "Total TTbar MC: "<< h_ttbar->Integral(0, h_ttbar->GetNbinsX()) << std::endl;
  std::cout << "Total WZ MC: "<< h_wz->Integral(0, h_wz->GetNbinsX()) << std::endl;
  std::cout << "Total ZZ MC: "<< h_zz->Integral(0, h_zz->GetNbinsX()) << std::endl;
  std::cout << "Total TTW MC: "<< h_ttw->Integral(0, h_ttw->GetNbinsX()) << std::endl;
  std::cout << "Total TTZ MC: "<< h_ttz->Integral(0, h_ttz->GetNbinsX()) << std::endl;

  // About to draw plots
  hs_background->Draw("HIST");
  h_data->Draw("pesame"); // "pex0same"
  h_systematic_error->Draw("E2same");

  // About to draw title
  TLatex *tex = new TLatex();
  tex->SetTextColor(1);
  tex->SetTextAlign(10);
  tex->SetTextSize(0.05);
  tex->SetTextSize(0.040);
  tex->SetTextFont(62); //42
  tex->SetLineWidth(2);
  tex->SetNDC();

  tex->DrawLatex(0.13, yposition, cmslabel);
  tex->DrawLatex(xposition, 0.915, "#sqrt{s} = 8 TeV, #scale[0.75]{#int}#it{L} d#it{t} = "+luminosity+" fb^{-1}");

  // About to draw legend  
  TLegend *leg = new TLegend(0.65, 0.45, 0.88, 0.87, "", "brNDC");
  leg->SetTextFont(42);
  leg->SetFillColor(0);
  leg->SetShadowColor(0);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.04);

  leg->AddEntry(h_data, "Data", "lep");
  leg->AddEntry(h_systematic_error, "Bkg uncertainty", "f");
  leg->AddEntry(h_ddbackground, "Data-driven" , "f");
  leg->AddEntry(h_ttbar, "t#bar{t}", "f");
  leg->AddEntry(h_wz, "WZ", "f");
  leg->AddEntry(h_zz, "ZZ", "f");
  leg->AddEntry(h_ttw, "t#bar{t}W", "f");
  leg->AddEntry(h_ttz, "t#bar{t}Z", "f");
  leg->AddEntry(h_higgs, "Higgs", "f");

  leg->Draw();
}

void ControlRegionAnalysis::makeZGammaControlPlots(std::vector<std::string> dataHist,
                                                   std::vector<std::string> backgroundHist)
{
  std::cout << "In ControlRegionAnalysis class makeZGammaControlPlots method!" << std::endl;

  // Reset ROOT
  gROOT->Reset();

  // Set sumw2 on by default and detach histogram from file by default
  TH1::SetDefaultSumw2(kTRUE);
  TH1::AddDirectory(kFALSE);

  TFile *f = new TFile(storefile.c_str());

  // Converting luminosity string into an integer [fb]
  double lumi;
  std::stringstream ss;
  ss << luminosity;
  ss >> lumi;

  TCanvas *c1 = new TCanvas("c1", "c1", 800, 600);
  c1->SetBorderMode(0);
  c1->SetFillColor(kWhite);
  c1->cd();

  // CFO uncertainties
  //Double_t cmu = 0.00714*0.5*(1.0465+0.74);
  Double_t cmu_err = 0.30;

  //Double_t ce = 0.00914*0.5*(0.6919+0.80);
  //Double_t ce_err = 0.30;

  //Double_t tau = 0.20*1.0768; // Low HT
  //Double_t tau = 0.05*0.96; // High HT
  //Double_t tau_err = 0.30;

  // Asymmetric Interanl Photon Conversion uncertainties
  //Double_t convmu = 0.00707;
  //Double_t convmu_err = 0.50;

  //Double_t conve = 0.0198;
  //Double_t conve_err = 0.5;

  // Data luminosity [pb]
  Double_t data_lumi = 1000*lumi;

  // Branching ratios
  Double_t WtoLNu = 0.3203;
  Double_t Wto2J = (1.0-0.3203);

  // Cross sections [pb]
  Double_t xsec_ttbar = 225.0;
  Double_t xsec_ttbar_fullyleptonic = pow(WtoLNu,2)*xsec_ttbar;
  Double_t xsec_ttbar_semileptonic = 2.0*WtoLNu*Wto2J*xsec_ttbar;
  Double_t xsec_wz = 32.3*(0.3257*0.10095)*1.20;
  Double_t xsec_zz = 0.92*17.7*(0.10095*0.10095)*1.08;
  Double_t xsec_ttw = 0.23;
  Double_t xsec_ttz = 0.208;

  // Number of generated events
  Double_t N_ttbar_fullyleptonic = 9.20104e+05+4.53536e+05+4.61283e+05+4.55184e+05+4.62597e+05+4.61627e+05+4.61345e+05+4.59597e+05+3.41874e+05;
  Double_t N_ttbar_semileptonic = 2.40481e+05+8.31427e+05+1.59079e+05+1.61002e+05+1.60029e+05+1.60359e+05+1.5889e+05+1.60191e+05+1.60265e+05+1.10807e+05;
  Double_t N_wz = 2.96073e+05+1.98916e+05+0.000000e+00;
  Double_t N_zz = 6.75305e+05+4.46457e+05+4.41662e+05+1.52382e+05;
  Double_t N_ttw = 2.17827e+05;
  Double_t N_ttz = 2.09677e+05;

  // Multilepton filtering efficiencies
  Double_t ttbar_filter_fullyleptonic = 0.3697;
  Double_t ttbar_filter_semileptonic = 0.064;
  Double_t wz_filter = 0.244;
  Double_t zz_filter = 0.358;

  // Scaling factors
  Double_t alpha_ttbar = (ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic)*data_lumi/(ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic/(ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic)*N_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic*(ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic)*N_ttbar_semileptonic);
  Double_t alpha_wz = wz_filter*xsec_wz*data_lumi/N_wz;
  Double_t alpha_zz = zz_filter*xsec_zz*data_lumi/N_zz;
  Double_t alpha_ttw = xsec_ttw*data_lumi/N_ttw;
  Double_t alpha_ttz = xsec_ttz*data_lumi/N_ttz;

  // Errors
  Double_t max_error = 0.0;
  Double_t total_error = 0.0;
  Double_t total_bin_error = 0.0;

  // Collision data
  TH1D *h_data = new TH1D();
  TH1D *h_ttbar = new TH1D();
  TH1D *h_wz = new TH1D();
  TH1D *h_zz = new TH1D();
  TH1D *h_ttw = new TH1D();
  TH1D *h_ttz = new TH1D();
  TH1D *h_higgs = new TH1D();
  TH1D *h_ddbackground = new TH1D();

  // For Data-driven contributions
  TH1D *h_ddCFOMu = new TH1D();
  TH1D *h_ddCFOEl = new TH1D();
  TH1D *h_ddCONVMu = new TH1D();
  TH1D *h_ddCONVEl = new TH1D();
  TH1D *h_ddTAU = new TH1D();

  // For Data-driven systematics
  TH1D *h_ddbackground_Err = new TH1D();

  // About to get histograms
  if (strParameters[2] == "2D") {
    // Collision data
    h_data = (TH1D *)((TH2D *)f->Get(dataHist[0].c_str()))->ProjectionX()->Rebin(5);

    // TTbar background
    h_ttbar = (TH1D *)((TH2D *)f->Get(backgroundHist[0].c_str()))->ProjectionX()->Rebin(5);

    // WZ background
    h_wz = (TH1D *)((TH2D *)f->Get(backgroundHist[1].c_str()))->ProjectionX()->Rebin(5);

    // ZZ background
    h_zz = (TH1D *)((TH2D *)f->Get(backgroundHist[2].c_str()))->ProjectionX()->Rebin(5);

    // TTW background
    h_ttw = (TH1D *)((TH2D *)f->Get(backgroundHist[3].c_str()))->ProjectionX()->Rebin(5);

    // TTZ background
    h_ttz = (TH1D *)((TH2D *)f->Get(backgroundHist[4].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven background
    h_ddbackground = (TH1D *)((TH2D *)f->Get(backgroundHist[5].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven ddCFOMu background
    h_ddCFOMu = (TH1D *)((TH2D *)f->Get(backgroundHist[6].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven h_ddCFOEl background
    h_ddCFOEl = (TH1D *)((TH2D *)f->Get(backgroundHist[7].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven ddCONVMu1D background
    h_ddCONVMu = (TH1D *)((TH2D *)f->Get(backgroundHist[8].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven h_ddCONVEl background
    h_ddCONVEl = (TH1D *)((TH2D *)f->Get(backgroundHist[9].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven h_ddCONVEl background
    h_ddTAU = (TH1D *)((TH2D *)f->Get(backgroundHist[10].c_str()))->ProjectionX()->Rebin(5);

    // Higgs background
    h_higgs = (TH1D *)((TH2D *)f->Get(backgroundHist[11].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven h_ddbackground_Err
    h_ddbackground_Err = (TH1D *)h_data->Clone();
    h_ddbackground_Err->Reset("ICES");
  }

  // Collision data
  h_data->SetMarkerStyle(8);
  h_data->SetFillColor(kBlack);
  h_data->SetLineColor(kBlack);

  // TTbar background
  h_ttbar->SetFillColor(kMagenta-7);
  h_ttbar->SetLineColor(kBlack);

  // WZ background
  h_wz->SetFillColor(kSpring+7);
  h_wz->SetLineColor(kBlack);

  // ZZ background
  h_zz->SetFillColor(kSpring+4);
  h_zz->SetLineColor(kBlack);

  // TTW background
  h_ttw->SetFillColor(kCyan-3);
  h_ttw->SetLineColor(kBlack);

  // TTZ background
  h_ttz->SetFillColor(kCyan);
  h_ttz->SetLineColor(kBlack);

  // Higgs background
  h_higgs->SetFillColor(kBlue);
  h_higgs->SetLineColor(kBlack);

  // Data-driven background
  h_ddbackground->SetFillColor(kYellow);
  h_ddbackground->SetLineColor(kBlack);
  h_ddCFOMu->SetFillColor(kYellow);
  h_ddCFOMu->SetLineColor(kBlack);
  h_ddCONVMu->SetFillColor(92);
  h_ddCONVMu->SetLineColor(kBlack);

  // Systematic errors
  TH1D *h_systematic_error = new TH1D("", "", 16, 0, 400);
  h_systematic_error->SetFillColor(kBlue-8);
  h_systematic_error->SetFillStyle(3344);
  h_systematic_error->SetMarkerStyle(0.0);
  
  h_systematic_error->Add(h_zz);
  h_systematic_error->Add(h_wz);
  h_systematic_error->Add(h_ttz);
  h_systematic_error->Add(h_ttw);
  h_systematic_error->Add(h_ttbar);
  h_systematic_error->Add(h_higgs);
  h_systematic_error->Add(h_ddCFOMu);
  h_systematic_error->Add(h_ddCFOEl);
  h_systematic_error->Add(h_ddTAU);
  h_systematic_error->Add(h_ddCONVMu);
  h_systematic_error->Add(h_ddCONVEl);

  // Setting systematic uncertainty
  for (Int_t i = 1; i < h_systematic_error->GetNbinsX(); i++) {

    h_ddbackground_Err->SetBinContent(i,cmu_err*h_ddbackground->GetBinContent(i));

    total_bin_error = sqrt(pow(h_ddbackground_Err->GetBinContent(i),2)+h_ddbackground->GetBinContent(i)+pow(0.50*h_ttbar->GetBinContent(i),2)+pow(0.06*h_ttbar->GetBinContent(i),2)+pow(alpha_ttbar*sqrt(h_ttbar->GetBinContent(i)),2)+pow(0.06*h_wz->GetBinContent(i),2)+pow(alpha_wz*sqrt(h_wz->GetBinContent(i)),2)+pow(0.12*h_zz->GetBinContent(i),2)+pow(alpha_zz*sqrt(h_zz->GetBinContent(i)),2)+pow(0.5*h_ttw->GetBinContent(i),2)+pow(alpha_ttw*sqrt(h_ttw->GetBinError(i)),2)+pow(0.5*h_ttz->GetBinContent(i),2)+pow(alpha_ttz*sqrt(h_ttz->GetBinContent(i)),2));

    h_systematic_error->SetBinError(i, total_bin_error);

    max_error = TMath::Max(max_error, total_bin_error);
  }

  // Calculating total error
  for (Int_t i = 1; i < h_systematic_error->GetNbinsX(); i++) {
      total_error += h_systematic_error->GetBinError(i);
  }

  // About to get stack
  THStack *hs_background = new THStack("hs_background", "");

  hs_background->Add(h_ddCFOMu);
  hs_background->Add(h_ddCFOEl);
  hs_background->Add(h_ddTAU);
  hs_background->Add(h_zz);
  hs_background->Add(h_wz);
  hs_background->Add(h_ttbar);
  hs_background->Add(h_ttz);
  hs_background->Add(h_ttw);
  hs_background->Add(h_higgs);
  hs_background->Add(h_ddCONVMu);
  hs_background->Add(h_ddCONVEl);

  // Setting maximum y-axis range
  Double_t yaxis_min = 0.0;
  Double_t yaxis_max = 1.0;

  yaxis_max = TMath::Max(h_data->GetMaximum(), hs_background->GetMaximum()+max_error);

  hs_background->SetMaximum(1.25*yaxis_max);
  hs_background->SetMinimum(yaxis_min);
  hs_background->Draw("HIST");

  hs_background->GetYaxis()->SetTitle(strParameters[0]);
  hs_background->GetYaxis()->SetTitleOffset(1.1);
  hs_background->GetYaxis()->SetTitleFont(42);
  hs_background->GetYaxis()->SetTitleSize(0.055);
  hs_background->GetYaxis()->SetLabelFont(42);
  hs_background->GetYaxis()->SetLabelSize(0.040);
  hs_background->GetXaxis()->SetTitle(strParameters[1]);
  hs_background->GetXaxis()->SetTitleOffset(1.2);
  hs_background->GetXaxis()->SetTitleFont(42);
  hs_background->GetXaxis()->SetTitleSize(0.055);
  hs_background->GetXaxis()->SetLabelFont(42);
  hs_background->GetXaxis()->SetLabelSize(0.040);

  // Print out statements
  std::cout << "Total Observation: "<< h_data->Integral(0, h_data->GetNbinsX()) << std::endl;
  std::cout << "Total Background: " << h_systematic_error->Integral(0, h_systematic_error->GetNbinsX()) << std::endl;
  std::cout << "Total Background Uncertainty: " << total_error << std::endl;
  std::cout << "Total Data-driven: "<< h_ddbackground->Integral(0, h_ddbackground->GetNbinsX()) << std::endl;
  std::cout << "Total TTbar MC: "<< h_ttbar->Integral(0, h_ttbar->GetNbinsX()) << std::endl;
  std::cout << "Total WZ MC: "<< h_wz->Integral(0, h_wz->GetNbinsX()) << std::endl;
  std::cout << "Total ZZ MC: "<< h_zz->Integral(0, h_zz->GetNbinsX()) << std::endl;
  std::cout << "Total TTW MC: "<< h_ttw->Integral(0, h_ttw->GetNbinsX()) << std::endl;
  std::cout << "Total TTZ MC: "<< h_ttz->Integral(0, h_ttz->GetNbinsX()) << std::endl;

  // About to draw plots
  hs_background->Draw("HIST");
  h_data->Draw("pesame"); // "pex0same"
  h_systematic_error->Draw("E2same");

  // About to draw title
  TLatex *tex = new TLatex();
  tex->SetTextColor(1);
  tex->SetTextAlign(10);
  tex->SetTextSize(0.05);
  tex->SetTextSize(0.040);
  tex->SetTextFont(62); //42
  tex->SetLineWidth(2);
  tex->SetNDC();

  tex->DrawLatex(0.13, yposition, cmslabel);
  tex->DrawLatex(xposition, 0.915, "#sqrt{s} = 8 TeV, #scale[0.75]{#int}#it{L} d#it{t} = "+luminosity+" fb^{-1}");

  // About to draw legend  
  TLegend *leg = new TLegend(0.65, 0.45, 0.88, 0.87, "", "brNDC");
  leg->SetTextFont(42);
  leg->SetFillColor(0);
  leg->SetShadowColor(0);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.04);

  leg->AddEntry(h_data, "Data", "lep");
  leg->AddEntry(h_systematic_error, "Bkg uncertainty", "f");
  leg->AddEntry(h_ddCONVMu, "#font[132]{#it{l}}#font[132]{#it{l}}+#gamma", "f");
  leg->AddEntry(h_ddCFOMu, "#font[132]{#it{l}}#font[132]{#it{l}}+jet", "f");
  leg->AddEntry(h_ttbar, "t#bar{t}", "f");
  leg->AddEntry(h_wz, "WZ", "f");
  leg->AddEntry(h_zz, "ZZ", "f");
  leg->AddEntry(h_ttw, "t#bar{t}W", "f");
  leg->AddEntry(h_ttz, "t#bar{t}Z", "f");
  leg->AddEntry(h_higgs, "Higgs", "f");

  leg->Draw();
}

void ControlRegionAnalysis::makeallplots()
{
  std::cout << "In ControlRegionAnalysis class makeallplots method!" << std::endl;

  ifstream in(strParameters[2].Data());

  // Vectors to store bin numbers
  vector< vector<int>* > BinCategories; vector<TString> BinCategoriesName;
  vector<int> BinsL3DY0Tau0b0; BinCategories.push_back(&BinsL3DY0Tau0b0); BinCategoriesName.push_back("L3DY0Tau0b0");
  vector<int> BinsL3DY0Tau1b0; BinCategories.push_back(&BinsL3DY0Tau1b0); BinCategoriesName.push_back("L3DY0Tau1b0");
  vector<int> BinsL3DY0Tau0b1; BinCategories.push_back(&BinsL3DY0Tau0b1); BinCategoriesName.push_back("L3DY0Tau0b1");
  vector<int> BinsL3DY0Tau1b1; BinCategories.push_back(&BinsL3DY0Tau1b1); BinCategoriesName.push_back("L3DY0Tau1b1");

  vector<int> BinsL3DY0Tau0b0HTV; BinCategories.push_back(&BinsL3DY0Tau0b0HTV); BinCategoriesName.push_back("L3DY0Tau0b0HTV");
  vector<int> BinsL3DY0Tau1b0HTV; BinCategories.push_back(&BinsL3DY0Tau1b0HTV); BinCategoriesName.push_back("L3DY0Tau1b0HTV");
  vector<int> BinsL3DY0Tau0b1HTV; BinCategories.push_back(&BinsL3DY0Tau0b1HTV); BinCategoriesName.push_back("L3DY0Tau0b1HTV");
  vector<int> BinsL3DY0Tau1b1HTV; BinCategories.push_back(&BinsL3DY0Tau1b1HTV); BinCategoriesName.push_back("L3DY0Tau1b1HTV");

  vector<int> BinsL3DY1Tau0b0; BinCategories.push_back(&BinsL3DY1Tau0b0); BinCategoriesName.push_back("L3DY1Tau0b0");
  vector<int> BinsL3DY1ZVZHTau0b0; BinCategories.push_back(&BinsL3DY1ZVZHTau0b0); BinCategoriesName.push_back("L3DY1ZVZHTau0b0");
  vector<int> BinsL3DY1ZVZLTau0b0; BinCategories.push_back(&BinsL3DY1ZVZLTau0b0); BinCategoriesName.push_back("L3DY1ZVZLTau0b0");

  vector<int> BinsL3DY1Tau0b0HTV; BinCategories.push_back(&BinsL3DY1Tau0b0HTV); BinCategoriesName.push_back("L3DY1Tau0b0HTV");
  vector<int> BinsL3DY1ZVZHTau0b0HTV; BinCategories.push_back(&BinsL3DY1ZVZHTau0b0HTV); BinCategoriesName.push_back("L3DY1ZVZHTau0b0HTV");
  vector<int> BinsL3DY1ZVZLTau0b0HTV; BinCategories.push_back(&BinsL3DY1ZVZLTau0b0HTV); BinCategoriesName.push_back("L3DY1ZVZLTau0b0HTV");

  vector<int> BinsL3DY1Tau0b1; BinCategories.push_back(&BinsL3DY1Tau0b1); BinCategoriesName.push_back("L3DY1Tau0b1");
  vector<int> BinsL3DY1ZVZHTau0b1; BinCategories.push_back(&BinsL3DY1ZVZHTau0b1); BinCategoriesName.push_back("L3DY1ZVZHTau0b1");
  vector<int> BinsL3DY1ZVZLTau0b1; BinCategories.push_back(&BinsL3DY1ZVZLTau0b1); BinCategoriesName.push_back("L3DY1ZVZLTau0b1");

  vector<int> BinsL3DY1Tau0b1HTV; BinCategories.push_back(&BinsL3DY1Tau0b1HTV); BinCategoriesName.push_back("L3DY1Tau0b1HTV");
  vector<int> BinsL3DY1ZVZHTau0b1HTV; BinCategories.push_back(&BinsL3DY1ZVZHTau0b1HTV); BinCategoriesName.push_back("L3DY1ZVZHTau0b1HTV");
  vector<int> BinsL3DY1ZVZLTau0b1HTV; BinCategories.push_back(&BinsL3DY1ZVZLTau0b1HTV); BinCategoriesName.push_back("L3DY1ZVZLTau0b1HTV");

  vector<int> BinsL3DY1Tau1b0; BinCategories.push_back(&BinsL3DY1Tau1b0); BinCategoriesName.push_back("L3DY1Tau1b0");
  vector<int> BinsL3DY1ZVZHTau1b0; BinCategories.push_back(&BinsL3DY1ZVZHTau1b0); BinCategoriesName.push_back("L3DY1ZVZHTau1b0");
  vector<int> BinsL3DY1ZVZLTau1b0; BinCategories.push_back(&BinsL3DY1ZVZLTau1b0); BinCategoriesName.push_back("L3DY1ZVZLTau1b0");

  vector<int> BinsL3DY1Tau1b0HTV; BinCategories.push_back(&BinsL3DY1Tau1b0HTV); BinCategoriesName.push_back("L3DY1Tau1b0HTV");
  vector<int> BinsL3DY1ZVZHTau1b0HTV; BinCategories.push_back(&BinsL3DY1ZVZHTau1b0HTV); BinCategoriesName.push_back("L3DY1ZVZHTau1b0HTV");
  vector<int> BinsL3DY1ZVZLTau1b0HTV; BinCategories.push_back(&BinsL3DY1ZVZLTau1b0HTV); BinCategoriesName.push_back("L3DY1ZVZLTau1b0HTV");

  vector<int> BinsL3DY1Tau1b1; BinCategories.push_back(&BinsL3DY1Tau1b1); BinCategoriesName.push_back("L3DY1Tau1b1");
  vector<int> BinsL3DY1ZVZHTau1b1; BinCategories.push_back(&BinsL3DY1ZVZHTau1b1); BinCategoriesName.push_back("L3DY1ZVZHTau1b1");
  vector<int> BinsL3DY1ZVZLTau1b1; BinCategories.push_back(&BinsL3DY1ZVZLTau1b1); BinCategoriesName.push_back("L3DY1ZVZLTau1b1");

  vector<int> BinsL3DY1Tau1b1HTV; BinCategories.push_back(&BinsL3DY1Tau1b1HTV); BinCategoriesName.push_back("L3DY1Tau1b1HTV");
  vector<int> BinsL3DY1ZVZHTau1b1HTV; BinCategories.push_back(&BinsL3DY1ZVZHTau1b1HTV); BinCategoriesName.push_back("L3DY1ZVZHTau1b1HTV");
  vector<int> BinsL3DY1ZVZLTau1b1HTV; BinCategories.push_back(&BinsL3DY1ZVZLTau1b1HTV); BinCategoriesName.push_back("L3DY1ZVZLTau1b1HTV");

  //Start 4-leptons
  vector<int> BinsL4DY0Tau0b0; BinCategories.push_back(&BinsL4DY0Tau0b0); BinCategoriesName.push_back("L4DY0Tau0b0");
  vector<int> BinsL4DY0Tau1b0; BinCategories.push_back(&BinsL4DY0Tau1b0); BinCategoriesName.push_back("L4DY0Tau1b0");
  vector<int> BinsL4DY0Tau0b1; BinCategories.push_back(&BinsL4DY0Tau0b1); BinCategoriesName.push_back("L4DY0Tau0b1");
  vector<int> BinsL4DY0Tau1b1; BinCategories.push_back(&BinsL4DY0Tau1b1); BinCategoriesName.push_back("L4DY0Tau1b1");
  
  vector<int> BinsL4DY0Tau0b0HTV; BinCategories.push_back(&BinsL4DY0Tau0b0HTV); BinCategoriesName.push_back("L4DY0Tau0b0HTV");
  vector<int> BinsL4DY0Tau1b0HTV; BinCategories.push_back(&BinsL4DY0Tau1b0HTV); BinCategoriesName.push_back("L4DY0Tau1b0HTV");
  vector<int> BinsL4DY0Tau0b1HTV; BinCategories.push_back(&BinsL4DY0Tau0b1HTV); BinCategoriesName.push_back("L4DY0Tau0b1HTV");
  vector<int> BinsL4DY0Tau1b1HTV; BinCategories.push_back(&BinsL4DY0Tau1b1HTV); BinCategoriesName.push_back("L4DY0Tau1b1HTV");

  vector<int> BinsL4DY1Tau0b0; BinCategories.push_back(&BinsL4DY1Tau0b0); BinCategoriesName.push_back("L4DY1Tau0b0");
  vector<int> BinsL4DY1ZVTau0b0; BinCategories.push_back(&BinsL4DY1ZVTau0b0); BinCategoriesName.push_back("L4DY1ZVTau0b0");

  vector<int> BinsL4DY1Tau0b0HTV; BinCategories.push_back(&BinsL4DY1Tau0b0HTV); BinCategoriesName.push_back("L4DY1Tau0b0HTV");
  vector<int> BinsL4DY1ZVTau0b0HTV; BinCategories.push_back(&BinsL4DY1ZVTau0b0HTV); BinCategoriesName.push_back("L4DY1ZVTau0b0HTV");

  vector<int> BinsL4DY1Tau0b1; BinCategories.push_back(&BinsL4DY1Tau0b1); BinCategoriesName.push_back("L4DY1Tau0b1");
  vector<int> BinsL4DY1ZVTau0b1; BinCategories.push_back(&BinsL4DY1ZVTau0b1); BinCategoriesName.push_back("L4DY1ZVTau0b1");

  vector<int> BinsL4DY1Tau0b1HTV; BinCategories.push_back(&BinsL4DY1Tau0b1HTV); BinCategoriesName.push_back("L4DY1Tau0b1HTV");
  vector<int> BinsL4DY1ZVTau0b1HTV; BinCategories.push_back(&BinsL4DY1ZVTau0b1HTV); BinCategoriesName.push_back("L4DY1ZVTau0b1HTV");

  vector<int> BinsL4DY1Tau1b0; BinCategories.push_back(&BinsL4DY1Tau1b0); BinCategoriesName.push_back("L4DY1Tau1b0");
  vector<int> BinsL4DY1ZVTau1b0; BinCategories.push_back(&BinsL4DY1ZVTau1b0); BinCategoriesName.push_back("L4DY1ZVTau1b0");

  vector<int> BinsL4DY1Tau1b0HTV; BinCategories.push_back(&BinsL4DY1Tau1b0HTV); BinCategoriesName.push_back("L4DY1Tau1b0HTV");
  vector<int> BinsL4DY1ZVTau1b0HTV; BinCategories.push_back(&BinsL4DY1ZVTau1b0HTV); BinCategoriesName.push_back("L4DY1ZVTau1b0HTV");

  vector<int> BinsL4DY1Tau1b1; BinCategories.push_back(&BinsL4DY1Tau1b1); BinCategoriesName.push_back("L4DY1Tau1b1");
  vector<int> BinsL4DY1ZVTau1b1; BinCategories.push_back(&BinsL4DY1ZVTau1b1); BinCategoriesName.push_back("L4DY1ZVTau1b1");

  vector<int> BinsL4DY1Tau1b1HTV; BinCategories.push_back(&BinsL4DY1Tau1b1HTV); BinCategoriesName.push_back("L4DY1Tau1b1HTV");
  vector<int> BinsL4DY1ZVTau1b1HTV; BinCategories.push_back(&BinsL4DY1ZVTau1b1HTV); BinCategoriesName.push_back("L4DY1ZVTau1b1HTV");

  //DY2
  vector<int> BinsL4DY2Tau0b0; BinCategories.push_back(&BinsL4DY2Tau0b0); BinCategoriesName.push_back("L4DY2Tau0b0");
  vector<int> BinsL4DY2ZVTau0b0; BinCategories.push_back(&BinsL4DY2ZVTau0b0); BinCategoriesName.push_back("L4DY2ZVTau0b0");

  vector<int> BinsL4DY2Tau0b0HTV; BinCategories.push_back(&BinsL4DY2Tau0b0HTV); BinCategoriesName.push_back("L4DY2Tau0b0HTV");
  vector<int> BinsL4DY2ZVTau0b0HTV; BinCategories.push_back(&BinsL4DY2ZVTau0b0HTV); BinCategoriesName.push_back("L4DY2ZVTau0b0HTV");

  vector<int> BinsL4DY2Tau0b1; BinCategories.push_back(&BinsL4DY2Tau0b1); BinCategoriesName.push_back("L4DY2Tau0b1");
  vector<int> BinsL4DY2ZVTau0b1; BinCategories.push_back(&BinsL4DY2ZVTau0b1); BinCategoriesName.push_back("L4DY2ZVTau0b1");

  vector<int> BinsL4DY2Tau0b1HTV; BinCategories.push_back(&BinsL4DY2Tau0b1HTV); BinCategoriesName.push_back("L4DY2Tau0b1HTV");
  vector<int> BinsL4DY2ZVTau0b1HTV; BinCategories.push_back(&BinsL4DY2ZVTau0b1HTV); BinCategoriesName.push_back("L4DY2ZVTau0b1HTV");

  vector< vector<string>* > BinNames;
  vector<string> NamesL3DY0Tau0b0; BinNames.push_back(&NamesL3DY0Tau0b0);
  vector<string> NamesL3DY0Tau1b0; BinNames.push_back(&NamesL3DY0Tau1b0);
  vector<string> NamesL3DY0Tau0b1; BinNames.push_back(&NamesL3DY0Tau0b1);
  vector<string> NamesL3DY0Tau1b1; BinNames.push_back(&NamesL3DY0Tau1b1);
  
  vector<string> NamesL3DY0Tau0b0HTV; BinNames.push_back(&NamesL3DY0Tau0b0HTV);
  vector<string> NamesL3DY0Tau1b0HTV; BinNames.push_back(&NamesL3DY0Tau1b0HTV);
  vector<string> NamesL3DY0Tau0b1HTV; BinNames.push_back(&NamesL3DY0Tau0b1HTV);
  vector<string> NamesL3DY0Tau1b1HTV; BinNames.push_back(&NamesL3DY0Tau1b1HTV);

  vector<string> NamesL3DY1Tau0b0; BinNames.push_back(&NamesL3DY1Tau0b0);
  vector<string> NamesL3DY1ZVZHTau0b0; BinNames.push_back(&NamesL3DY1ZVZHTau0b0);
  vector<string> NamesL3DY1ZVZLTau0b0; BinNames.push_back(&NamesL3DY1ZVZLTau0b0);
  
  vector<string> NamesL3DY1Tau0b0HTV; BinNames.push_back(&NamesL3DY1Tau0b0HTV);
  vector<string> NamesL3DY1ZVZHTau0b0HTV; BinNames.push_back(&NamesL3DY1ZVZHTau0b0HTV);
  vector<string> NamesL3DY1ZVZLTau0b0HTV; BinNames.push_back(&NamesL3DY1ZVZLTau0b0HTV);

  vector<string> NamesL3DY1Tau0b1; BinNames.push_back(&NamesL3DY1Tau0b1);
  vector<string> NamesL3DY1ZVZHTau0b1; BinNames.push_back(&NamesL3DY1ZVZHTau0b1);
  vector<string> NamesL3DY1ZVZLTau0b1; BinNames.push_back(&NamesL3DY1ZVZLTau0b1);
  
  vector<string> NamesL3DY1Tau0b1HTV; BinNames.push_back(&NamesL3DY1Tau0b1HTV);
  vector<string> NamesL3DY1ZVZHTau0b1HTV; BinNames.push_back(&NamesL3DY1ZVZHTau0b1HTV);
  vector<string> NamesL3DY1ZVZLTau0b1HTV; BinNames.push_back(&NamesL3DY1ZVZLTau0b1HTV);

  vector<string> NamesL3DY1Tau1b0; BinNames.push_back(&NamesL3DY1Tau1b0);
  vector<string> NamesL3DY1ZVZHTau1b0; BinNames.push_back(&NamesL3DY1ZVZHTau1b0);
  vector<string> NamesL3DY1ZVZLTau1b0; BinNames.push_back(&NamesL3DY1ZVZLTau1b0);
  
  vector<string> NamesL3DY1Tau1b0HTV; BinNames.push_back(&NamesL3DY1Tau1b0HTV);
  vector<string> NamesL3DY1ZVZHTau1b0HTV; BinNames.push_back(&NamesL3DY1ZVZHTau1b0HTV);
  vector<string> NamesL3DY1ZVZLTau1b0HTV; BinNames.push_back(&NamesL3DY1ZVZLTau1b0HTV);

  vector<string> NamesL3DY1Tau1b1; BinNames.push_back(&NamesL3DY1Tau1b1);
  vector<string> NamesL3DY1ZVZHTau1b1; BinNames.push_back(&NamesL3DY1ZVZHTau1b1);
  vector<string> NamesL3DY1ZVZLTau1b1; BinNames.push_back(&NamesL3DY1ZVZLTau1b1);
  
  vector<string> NamesL3DY1Tau1b1HTV; BinNames.push_back(&NamesL3DY1Tau1b1HTV);
  vector<string> NamesL3DY1ZVZHTau1b1HTV; BinNames.push_back(&NamesL3DY1ZVZHTau1b1HTV);
  vector<string> NamesL3DY1ZVZLTau1b1HTV; BinNames.push_back(&NamesL3DY1ZVZLTau1b1HTV);

  //4-leptons
  vector<string> NamesL4DY0Tau0b0; BinNames.push_back(&NamesL4DY0Tau0b0);
  vector<string> NamesL4DY0Tau1b0; BinNames.push_back(&NamesL4DY0Tau1b0);
  vector<string> NamesL4DY0Tau0b1; BinNames.push_back(&NamesL4DY0Tau0b1);
  vector<string> NamesL4DY0Tau1b1; BinNames.push_back(&NamesL4DY0Tau1b1);

  vector<string> NamesL4DY0Tau0b0HTV; BinNames.push_back(&NamesL4DY0Tau0b0HTV);
  vector<string> NamesL4DY0Tau1b0HTV; BinNames.push_back(&NamesL4DY0Tau1b0HTV);
  vector<string> NamesL4DY0Tau0b1HTV; BinNames.push_back(&NamesL4DY0Tau0b1HTV);
  vector<string> NamesL4DY0Tau1b1HTV; BinNames.push_back(&NamesL4DY0Tau1b1HTV);

  vector<string> NamesL4DY1Tau0b0; BinNames.push_back(&NamesL4DY1Tau0b0);
  vector<string> NamesL4DY1ZVTau0b0; BinNames.push_back(&NamesL4DY1ZVTau0b0);
  
  vector<string> NamesL4DY1Tau0b0HTV; BinNames.push_back(&NamesL4DY1Tau0b0HTV);
  vector<string> NamesL4DY1ZVTau0b0HTV; BinNames.push_back(&NamesL4DY1ZVTau0b0HTV);
  
  vector<string> NamesL4DY1Tau0b1; BinNames.push_back(&NamesL4DY1Tau0b1);
  vector<string> NamesL4DY1ZVTau0b1; BinNames.push_back(&NamesL4DY1ZVTau0b1);
  
  vector<string> NamesL4DY1Tau0b1HTV; BinNames.push_back(&NamesL4DY1Tau0b1HTV);
  vector<string> NamesL4DY1ZVTau0b1HTV; BinNames.push_back(&NamesL4DY1ZVTau0b1HTV);
  
  vector<string> NamesL4DY1Tau1b0; BinNames.push_back(&NamesL4DY1Tau1b0);
  vector<string> NamesL4DY1ZVTau1b0; BinNames.push_back(&NamesL4DY1ZVTau1b0);
  
  vector<string> NamesL4DY1Tau1b0HTV; BinNames.push_back(&NamesL4DY1Tau1b0HTV);
  vector<string> NamesL4DY1ZVTau1b0HTV; BinNames.push_back(&NamesL4DY1ZVTau1b0HTV);
  
  vector<string> NamesL4DY1Tau1b1; BinNames.push_back(&NamesL4DY1Tau1b1);
  vector<string> NamesL4DY1ZVTau1b1; BinNames.push_back(&NamesL4DY1ZVTau1b1);
  
  vector<string> NamesL4DY1Tau1b1HTV; BinNames.push_back(&NamesL4DY1Tau1b1HTV);
  vector<string> NamesL4DY1ZVTau1b1HTV; BinNames.push_back(&NamesL4DY1ZVTau1b1HTV);
  
  vector<string> NamesL4DY2Tau0b0; BinNames.push_back(&NamesL4DY2Tau0b0);
  vector<string> NamesL4DY2ZVTau0b0; BinNames.push_back(&NamesL4DY2ZVTau0b0);
  
  vector<string> NamesL4DY2Tau0b0HTV; BinNames.push_back(&NamesL4DY2Tau0b0HTV);
  vector<string> NamesL4DY2ZVTau0b0HTV; BinNames.push_back(&NamesL4DY2ZVTau0b0HTV);
  
  vector<string> NamesL4DY2Tau0b1; BinNames.push_back(&NamesL4DY2Tau0b1);
  vector<string> NamesL4DY2ZVTau0b1; BinNames.push_back(&NamesL4DY2ZVTau0b1);
  
  vector<string> NamesL4DY2Tau0b1HTV; BinNames.push_back(&NamesL4DY2Tau0b1HTV);
  vector<string> NamesL4DY2ZVTau0b1HTV; BinNames.push_back(&NamesL4DY2ZVTau0b1HTV);

  // Temporary variables
  string line;
  char channelname[100]; int Nlepton, Ntau, Nbjet, NDY, NST, NMET, NHT, Nbin;

  while(getline(in, line))
    {
      sscanf(line.c_str(), "%s %i %i %i %i %i %i %i %i", channelname, &Nlepton, &Ntau, &Nbjet, &NDY, &NST, &NMET, &NHT, &Nbin);

      cout << channelname << " " << Nlepton << " " << Ntau << " " << Nbjet << " " << NDY 
           << " " << NST << " " << NMET << " " << NHT << " " << Nbin << endl;

      if(NST==1)//MTW:0-50
      {
      if(Nlepton==3 && NDY==0 && Ntau==0 && Nbjet==0 && NHT==0) 
	{
	  BinsL3DY0Tau0b0.push_back(Nbin);
	  NamesL3DY0Tau0b0.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==0 && Ntau==1 && Nbjet==0 && NHT==0) 
	{
	  BinsL3DY0Tau1b0.push_back(Nbin);
	  NamesL3DY0Tau1b0.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==0 && Ntau==0 && Nbjet==1 && NHT==0) 
	{
	  BinsL3DY0Tau0b1.push_back(Nbin);
	  NamesL3DY0Tau0b1.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==0 && Ntau==1 && Nbjet==1 && NHT==0) 
	{
	  BinsL3DY0Tau1b1.push_back(Nbin);
	  NamesL3DY0Tau1b1.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==10 && Ntau==0 && Nbjet==0 && NHT==0) 
	{
	  BinsL3DY1Tau0b0.push_back(Nbin);
	  NamesL3DY1Tau0b0.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==12 && Ntau==0 && Nbjet==0 && NHT==0) 
	{
	  BinsL3DY1ZVZHTau0b0.push_back(Nbin);
	  NamesL3DY1ZVZHTau0b0.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==11 && Ntau==0 && Nbjet==0 && NHT==0) 
	{
	  BinsL3DY1ZVZLTau0b0.push_back(Nbin);
	  NamesL3DY1ZVZLTau0b0.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==10 && Ntau==0 && Nbjet==1 && NHT==0) 
	{
	  BinsL3DY1Tau0b1.push_back(Nbin);
	  NamesL3DY1Tau0b1.push_back(channelname);
	}	  
      else if(Nlepton==3 && NDY==12 && Ntau==0 && Nbjet==1 && NHT==0)
	{
	  BinsL3DY1ZVZHTau0b1.push_back(Nbin);
	  NamesL3DY1ZVZHTau0b1.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==11 && Ntau==0 && Nbjet==1 && NHT==0) 
	{
	  BinsL3DY1ZVZLTau0b1.push_back(Nbin);
	  NamesL3DY1ZVZLTau0b1.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==10 && Ntau==1 && Nbjet==0 && NHT==0) 
	{
	  BinsL3DY1Tau1b0.push_back(Nbin);
	  NamesL3DY1Tau1b0.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==12 && Ntau==1 && Nbjet==0 && NHT==0)
	{
	  BinsL3DY1ZVZHTau1b0.push_back(Nbin);
	  NamesL3DY1ZVZHTau1b0.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==11 && Ntau==1 && Nbjet==0 && NHT==0) 
	{
	  BinsL3DY1ZVZLTau1b0.push_back(Nbin);
	  NamesL3DY1ZVZLTau1b0.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==10 && Ntau==1 && Nbjet==1 && NHT==0) 
	{
	  BinsL3DY1Tau1b1.push_back(Nbin);
	  NamesL3DY1Tau1b1.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==12 && Ntau==1 && Nbjet==1 && NHT==0) 
	{
	  BinsL3DY1ZVZHTau1b1.push_back(Nbin);
	  NamesL3DY1ZVZHTau1b1.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==11 && Ntau==1 && Nbjet==1 && NHT==0) 
	{
	  BinsL3DY1ZVZLTau1b1.push_back(Nbin);
	  NamesL3DY1ZVZLTau1b1.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==0 && Ntau==0 && Nbjet==0 && NHT==1) //Starting high HT bins
	{
	  BinsL3DY0Tau0b0HTV.push_back(Nbin);
	  NamesL3DY0Tau0b0HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==0 && Ntau==1 && Nbjet==0 && NHT==1) 
	{
	  BinsL3DY0Tau1b0HTV.push_back(Nbin);
	  NamesL3DY0Tau1b0HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==0 && Ntau==0 && Nbjet==1 && NHT==1) 
	{
	  BinsL3DY0Tau0b1HTV.push_back(Nbin);
	  NamesL3DY0Tau0b1HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==0 && Ntau==1 && Nbjet==1 && NHT==1) 
	{
	  BinsL3DY0Tau1b1HTV.push_back(Nbin);
	  NamesL3DY0Tau1b1HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==10 && Ntau==0 && Nbjet==0 && NHT==1) 
	{
	  BinsL3DY1Tau0b0HTV.push_back(Nbin);
	  NamesL3DY1Tau0b0HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==12 && Ntau==0 && Nbjet==0 && NHT==1) 
	{
	  BinsL3DY1ZVZHTau0b0HTV.push_back(Nbin);
	  NamesL3DY1ZVZHTau0b0HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==11 && Ntau==0 && Nbjet==0 && NHT==1) 
	{
	  BinsL3DY1ZVZLTau0b0HTV.push_back(Nbin);
	  NamesL3DY1ZVZLTau0b0HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==10 && Ntau==0 && Nbjet==1 && NHT==1) 
	{
	  BinsL3DY1Tau0b1HTV.push_back(Nbin);
	  NamesL3DY1Tau0b1HTV.push_back(channelname);
	}	  
      else if(Nlepton==3 && NDY==12 && Ntau==0 && Nbjet==1 && NHT==1)
	{
	  BinsL3DY1ZVZHTau0b1HTV.push_back(Nbin);
	  NamesL3DY1ZVZHTau0b1HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==11 && Ntau==0 && Nbjet==1 && NHT==1) 
	{
	  BinsL3DY1ZVZLTau0b1HTV.push_back(Nbin);
	  NamesL3DY1ZVZLTau0b1HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==10 && Ntau==1 && Nbjet==0 && NHT==1) 
	{
	  BinsL3DY1Tau1b0HTV.push_back(Nbin);
	  NamesL3DY1Tau1b0HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==12 && Ntau==1 && Nbjet==0 && NHT==1)
	{
	  BinsL3DY1ZVZHTau1b0HTV.push_back(Nbin);
	  NamesL3DY1ZVZHTau1b0HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==11 && Ntau==1 && Nbjet==0 && NHT==1) 
	{
	  BinsL3DY1ZVZLTau1b0HTV.push_back(Nbin);
	  NamesL3DY1ZVZLTau1b0HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==10 && Ntau==1 && Nbjet==1 && NHT==1) 
	{
	  BinsL3DY1Tau1b1HTV.push_back(Nbin);
	  NamesL3DY1Tau1b1HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==12 && Ntau==1 && Nbjet==1 && NHT==1) 
	{
	  BinsL3DY1ZVZHTau1b1HTV.push_back(Nbin);
	  NamesL3DY1ZVZHTau1b1HTV.push_back(channelname);
	}
      else if(Nlepton==3 && NDY==11 && Ntau==1 && Nbjet==1 && NHT==1) 
	{
	  BinsL3DY1ZVZLTau1b1HTV.push_back(Nbin);
	  NamesL3DY1ZVZLTau1b1HTV.push_back(channelname);
	}
      //4-leptons
      else if(Nlepton==4 && NDY==0 && Ntau==0 && Nbjet==0 && NHT==0) 
	{
	  BinsL4DY0Tau0b0.push_back(Nbin);
	  NamesL4DY0Tau0b0.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==0 && Ntau==1 && Nbjet==0 && NHT==0) 
	{
	  BinsL4DY0Tau1b0.push_back(Nbin);
	  NamesL4DY0Tau1b0.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==0 && Ntau==0 && Nbjet==1 && NHT==0) 
	{
	  BinsL4DY0Tau0b1.push_back(Nbin);
	  NamesL4DY0Tau0b1.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==0 && Ntau==1 && Nbjet==1 && NHT==0) 
	{
	  BinsL4DY0Tau1b1.push_back(Nbin);
	  NamesL4DY0Tau1b1.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==0 && Ntau==0 && Nbjet==0 && NHT==1) //Starting high HT bins
	{
	  BinsL4DY0Tau0b0HTV.push_back(Nbin);
	  NamesL4DY0Tau0b0HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==0 && Ntau==1 && Nbjet==0 && NHT==1) 
	{
	  BinsL4DY0Tau1b0HTV.push_back(Nbin);
	  NamesL4DY0Tau1b0HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==0 && Ntau==0 && Nbjet==1 && NHT==1) 
	{
	  BinsL4DY0Tau0b1HTV.push_back(Nbin);
	  NamesL4DY0Tau0b1HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==0 && Ntau==1 && Nbjet==1 && NHT==1) 
	{
	  BinsL4DY0Tau1b1HTV.push_back(Nbin);
	  NamesL4DY0Tau1b1HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==10 && Ntau==0 && Nbjet==0 && NHT==0)//DY1
	{
	  BinsL4DY1Tau0b0.push_back(Nbin);
	  NamesL4DY1Tau0b0.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==13 && Ntau==0 && Nbjet==0 && NHT==0) 
	{
	  BinsL4DY1ZVTau0b0.push_back(Nbin);
	  NamesL4DY1ZVTau0b0.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==10 && Ntau==0 && Nbjet==1 && NHT==0) 
	{
	  BinsL4DY1Tau0b1.push_back(Nbin);
	  NamesL4DY1Tau0b1.push_back(channelname);
	}	
      else if(Nlepton==4 && NDY==13 && Ntau==0 && Nbjet==1 && NHT==0) 
	{
	  BinsL4DY1ZVTau0b1.push_back(Nbin);
	  NamesL4DY1ZVTau0b1.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==10 && Ntau==1 && Nbjet==0 && NHT==0) 
	{
	  BinsL4DY1Tau1b0.push_back(Nbin);
	  NamesL4DY1Tau1b0.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==13 && Ntau==1 && Nbjet==0 && NHT==0) 
	{
	  BinsL4DY1ZVTau1b0.push_back(Nbin);
	  NamesL4DY1ZVTau1b0.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==10 && Ntau==1 && Nbjet==1 && NHT==0) 
	{
	  BinsL4DY1Tau1b1.push_back(Nbin);
	  NamesL4DY1Tau1b1.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==13 && Ntau==1 && Nbjet==1 && NHT==0) 
	{
	  BinsL4DY1ZVTau1b1.push_back(Nbin);
	  NamesL4DY1ZVTau1b1.push_back(channelname);
	}
      //here
      else if(Nlepton==4 && NDY==10 && Ntau==0 && Nbjet==0 && NHT==1) 
	{
	  BinsL4DY1Tau0b0HTV.push_back(Nbin);
	  NamesL4DY1Tau0b0HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==13 && Ntau==0 && Nbjet==0 && NHT==1) 
	{
	  BinsL4DY1ZVTau0b0HTV.push_back(Nbin);
	  NamesL4DY1ZVTau0b0HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==10 && Ntau==0 && Nbjet==1 && NHT==1) 
	{
	  BinsL4DY1Tau0b1HTV.push_back(Nbin);
	  NamesL4DY1Tau0b1HTV.push_back(channelname);
	}	
      else if(Nlepton==4 && NDY==13 && Ntau==0 && Nbjet==1 && NHT==1) 
	{
	  BinsL4DY1ZVTau0b1HTV.push_back(Nbin);
	  NamesL4DY1ZVTau0b1HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==10 && Ntau==1 && Nbjet==0 && NHT==1) 
	{
	  BinsL4DY1Tau1b0HTV.push_back(Nbin);
	  NamesL4DY1Tau1b0HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==13 && Ntau==1 && Nbjet==0 && NHT==1) 
	{
	  BinsL4DY1ZVTau1b0HTV.push_back(Nbin);
	  NamesL4DY1ZVTau1b0HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==10 && Ntau==1 && Nbjet==1 && NHT==1) 
	{
	  BinsL4DY1Tau1b1HTV.push_back(Nbin);
	  NamesL4DY1Tau1b1HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==13 && Ntau==1 && Nbjet==1 && NHT==1) 
	{
	  BinsL4DY1ZVTau1b1HTV.push_back(Nbin);
	  NamesL4DY1ZVTau1b1HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==20 && Ntau==0 && Nbjet==0 && NHT==0)//DY2
	{
	  BinsL4DY2Tau0b0.push_back(Nbin);
	  NamesL4DY2Tau0b0.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==21 && Ntau==0 && Nbjet==0 && NHT==0) 
	{
	  BinsL4DY2ZVTau0b0.push_back(Nbin);
	  NamesL4DY2ZVTau0b0.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==20 && Ntau==0 && Nbjet==1 && NHT==0) 
	{
	  BinsL4DY2Tau0b1.push_back(Nbin);
	  NamesL4DY2Tau0b1.push_back(channelname);
	}	
      else if(Nlepton==4 && NDY==21 && Ntau==0 && Nbjet==1 && NHT==0) 
	{
	  BinsL4DY2ZVTau0b1.push_back(Nbin);
	  NamesL4DY2ZVTau0b1.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==20 && Ntau==0 && Nbjet==0 && NHT==1) 
	{
	  BinsL4DY2Tau0b0HTV.push_back(Nbin);
	  NamesL4DY2Tau0b0HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==21 && Ntau==0 && Nbjet==0 && NHT==1) 
	{
	  BinsL4DY2ZVTau0b0HTV.push_back(Nbin);
	  NamesL4DY2ZVTau0b0HTV.push_back(channelname);
	}
      else if(Nlepton==4 && NDY==20 && Ntau==0 && Nbjet==1 && NHT==1) 
	{
	  BinsL4DY2Tau0b1HTV.push_back(Nbin);
	  NamesL4DY2Tau0b1HTV.push_back(channelname);
	}	
      else if(Nlepton==4 && NDY==21 && Ntau==0 && Nbjet==1 && NHT==1) 
	{
	  BinsL4DY2ZVTau0b1HTV.push_back(Nbin);
	  NamesL4DY2ZVTau0b1HTV.push_back(channelname);
	}
      else std::cout << "SRA: Something Wrong!" << std::endl;
      }
    } 
  //========================================End of channel names (bins)=================================================

  for(int i = 0; i < (int)BinCategories.size(); i++)
    {
      vector<string> names = *(BinNames[i]);
      std::cout << "Plot: " << BinCategoriesName[i] << " " << names.size() << std::endl;
      for(int j = 0; j < (int)names.size(); j++) std::cout << names[j] << std::endl;

      if(i==8) drawBins(*BinCategories[i], BinCategoriesName[i], 0, "b'b': BF(b'#rightarrowbZ)=50% @ 550 GeV");
    }
}

void ControlRegionAnalysis::drawBins(vector<Int_t> binnumbers, TString plotname, Int_t include_signal, TString signalname)
{
  std::cout << "In ControlRegionAnalysis class drawBins method!" << std::endl;

  // Bins for ST
  Double_t STbins[31] = {0,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,210,220,230,240,250,260,270,280,290,300};

  std::cout << "Making plot: " << plotname << " signalname: " << signalname << std::endl;
  //==========================================Open relevant files=========================================================
  TFile *f = TFile::Open(storefile.c_str());
  //TFile *g = new TFile();

  //if(include_signal == 1) g = TFile::Open("METHT_bprime550br50.root");
  //======================================End of opening relevant files==================================================  

  //========================================Get data source histograms====================================================
  vector<TH1D *> ddirrhist;
  vector<TString> ddirrnames;
  vector<Int_t> ddirrcolors;
  vector<Double_t> ddirrfudge;

  TH1D *TTbar0_ddirr = (TH1D*) f->Get("TTbar0_ddirr");
  TH1D *WZ18_ddirr = (TH1D*) f->Get("WZ18_ddirr");
  TH1D *ZZ20_ddirr = (TH1D*) f->Get("ZZ20_ddirr");
  TH1D *TTW23_ddirr = (TH1D*) f->Get("TTW23_ddirr");
  TH1D *TTZ24_ddirr = (TH1D*) f->Get("TTZ24_ddirr");
 
  // Things pushed back first appear lower
  ddirrhist.push_back(TTZ24_ddirr); ddirrnames.push_back("t#bar{t}Z"); ddirrcolors.push_back(kCyan); ddirrfudge.push_back(1.0);
  ddirrhist.push_back(TTW23_ddirr); ddirrnames.push_back("t#bar{t}W"); ddirrcolors.push_back(kCyan-3); ddirrfudge.push_back(1.0);
  ddirrhist.push_back(ZZ20_ddirr); ddirrnames.push_back("ZZ"); ddirrcolors.push_back(kSpring+4); ddirrfudge.push_back(1.0);
  ddirrhist.push_back(WZ18_ddirr); ddirrnames.push_back("WZ"); ddirrcolors.push_back(kSpring+7); ddirrfudge.push_back(1.0);
  ddirrhist.push_back(TTbar0_ddirr); ddirrnames.push_back("t#bar{t}"); ddirrcolors.push_back(kMagenta-7); ddirrfudge.push_back(1.0);

  //======================================End of getting data source histograms============================================

  //========================================Data and DD Bkg histograms=====================================================
  Double_t datafudge = 1.0;
  TH1D *data = (TH1D*) f->Get("data");
  data->SetMarkerStyle(8);

  Double_t ddbkgfudge = 1.0;
  TH1D *ddbkg = (TH1D *)f->Get("ddbkg");
  ddbkg->SetFillColor(kYellow);

  TH1D *totalbkgerror = (TH1D *)f->Get("Background_error");
  //==========================================End of getting data and dd bkg histograms====================================

  //=============================================Signal histograms=========================================================
  //TH1D *signal = new TH1D();
  if(include_signal == 1)
    {
      //signal = (TH1D*) g->Get("Signal");
      //signal->SetFillStyle(3002);
      //signal->SetFillColor(6);
    }
  //==========================================End of getting signal histograms============================================

  //=============================================Make TCanvas==============================================================
  TCanvas *c1 = new TCanvas("c1", "c1", 800, 600);
  c1->cd();
  //c1->SetLogy();
  //========================================End of making TLegend==========================================================

  //=======================================Generating all data-driven Bkg histograms=======================================
  TH1D *ddirrTempHistCol[ddirrhist.size()]; //array to store DD histograms with selected bins

  // Data histograms for these channels
  TString dataTEMPname = plotname + "_data";

  TH1D *dataTemp = new TH1D(dataTEMPname, dataTEMPname, binnumbers.size(), 0, binnumbers.size());
  dataTemp->SetMarkerStyle(8);
  dataTemp->SetBins(30, STbins);
  dataTemp->GetXaxis()->SetBinLabel(1,"");

  // DD bkg histogram for these channels
  TString ddbkgTEMPname = plotname + "_ddbkg";
  TH1D *ddbkgTEMP = new TH1D(ddbkgTEMPname, ddbkgTEMPname, binnumbers.size(), 0, binnumbers.size());
  ddbkgTEMP->SetBins(30, STbins);
  ddbkgTEMP->SetMinimum(5.0e-4);

  // Total Bkg error histogram for these channels
  TString totalbkgerrorTEMPname = plotname + "_totalbkgerror";
  TH1D *totalbkgerrorTEMP = new TH1D(totalbkgerrorTEMPname, totalbkgerrorTEMPname, binnumbers.size(), 0, binnumbers.size());
  totalbkgerrorTEMP->SetBins(30, STbins);

  // Signal histogram
  //TH1D *signalTEMP;
  if(1==include_signal)
    {
      //TString signalTEMPname = plotname + "_" + signalname;
      //signalTEMP = new TH1D(signalTEMPname, signalTEMPname, binnumbers.size(), 0, binnumbers.size());
      //signalTEMP->SetFillStyle(3409);//3001
      //signalTEMP->SetFillColor(kRed-9);      
      //signalTEMP->SetBins(30, STbins);
    }

  // Fill histograms
  Double_t datamax = -1;

  // Pick the right bins for the data histograms
  for(Int_t k = 0; k < (Int_t)binnumbers.size();k++)//loop over every bin
    {
      dataTemp->SetBinContent(k+1, datafudge*((Double_t) data->GetBinContent(binnumbers[k])));
      dataTemp->SetBinError(k+1, sqrt(datafudge)*sqrt((Double_t) data->GetBinContent(binnumbers[k])));
      ddbkgTEMP->SetBinContent(k+1, ddbkgfudge*((Double_t) ddbkg->GetBinContent(binnumbers[k])));
      if(dataTemp->GetBinContent(k+1)>datamax) datamax = dataTemp->GetBinContent(k+1);
      totalbkgerrorTEMP->SetBinContent(k+1, (Double_t) (totalbkgerror->GetBinContent(binnumbers[k])));

      if(include_signal == 1)
	{
          //signalTEMP->SetBinContent(k+1, (Double_t) (signal->GetBinContent(binnumbers[k])));
	}
    }

  // Pick the right bins for the ddirr histograms
  for(Int_t j = 0; j < (Int_t)ddirrhist.size(); j++) //Go over every ddirr histogram (TTbar, FSR)
    {
      stringstream ss; ss << j; TString index; ss >> index; ss.clear();
      TString ddirrhistTEMPname = plotname + "_" + ddirrnames[j] + index + "_ddirr";
      std::cout << ddirrhistTEMPname << std::endl;
      TH1D *ddirrhistTEMP = new TH1D(ddirrhistTEMPname, ddirrhistTEMPname, binnumbers.size(), 0, binnumbers.size());
      ddirrhistTEMP->SetBins(30, STbins);
      for(Int_t k = 0; k < (Int_t)binnumbers.size(); k++)//go over every bin of interest
	{
	  ddirrhistTEMP->SetBinContent(k+1, ddirrfudge[j]*((Double_t) ddirrhist[j]->GetBinContent(binnumbers[k])));
	}
      ddirrTempHistCol[j] = ddirrhistTEMP;
    }

  // Total bkg histogram
  TString totalbkgTEMPname = plotname + "_totalbkg";

  TH1D *totalbkg = new TH1D(totalbkgTEMPname, totalbkgTEMPname, binnumbers.size(), 0, binnumbers.size());
  totalbkg->SetFillColor(kBlue-8);
  totalbkg->SetFillStyle(3344);
  totalbkg->SetMarkerStyle(0.0);
  totalbkg->SetBins(30, STbins);

  for(Int_t ddirr = 0; ddirr < (Int_t)ddirrhist.size(); ddirr++)//loop over all MC backgrounds
    {
      totalbkg->Add(ddirrTempHistCol[ddirr]);
    }	
  totalbkg->Add(ddbkgTEMP);//Add data-driven bkg to all MC bkg

  // Make stack
  TString stackname = plotname + "_stackddirr";
  THStack *stackplotsddirr = new THStack(stackname, stackname);

// Better stack odering

  ddirrTempHistCol[0]->SetFillColor(ddirrcolors[0]); // TTZ
  stackplotsddirr->Add(ddirrTempHistCol[0]);

  ddirrTempHistCol[1]->SetFillColor(ddirrcolors[1]); // TTW
  stackplotsddirr->Add(ddirrTempHistCol[1]);

  ddirrTempHistCol[4]->SetFillColor(ddirrcolors[4]); // TTbar
  stackplotsddirr->Add(ddirrTempHistCol[4]);

  ddirrTempHistCol[2]->SetFillColor(ddirrcolors[2]); // ZZ
  stackplotsddirr->Add(ddirrTempHistCol[2]);

  // Color of data-driven backgrounds
  ddbkgTEMP->SetFillColor(kYellow); // Data-Driven Background
  stackplotsddirr->Add(ddbkgTEMP);

  ddirrTempHistCol[3]->SetFillColor(ddirrcolors[3]); // WZ
  stackplotsddirr->Add(ddirrTempHistCol[3]);


// Sanjay stacking order
/*
  ddirrTempHistCol[0]->SetFillColor(ddirrcolors[0]); // TTZ
  stackplotsddirr->Add(ddirrTempHistCol[0]);

  ddirrTempHistCol[1]->SetFillColor(ddirrcolors[1]); // TTW
  stackplotsddirr->Add(ddirrTempHistCol[1]);

  ddirrTempHistCol[2]->SetFillColor(ddirrcolors[2]); // ZZ
  stackplotsddirr->Add(ddirrTempHistCol[2]);

  ddirrTempHistCol[3]->SetFillColor(ddirrcolors[3]); // WZ
  stackplotsddirr->Add(ddirrTempHistCol[3]);

  ddirrTempHistCol[4]->SetFillColor(ddirrcolors[4]); // TTbar
  stackplotsddirr->Add(ddirrTempHistCol[4]);

  // Color of data-driven backgrounds
  ddbkgTEMP->SetFillColor(kYellow); // Data-Driven Background
  stackplotsddirr->Add(ddbkgTEMP);
*/
  //if(include_signal == 1) stackplotsddirr->Add(signalTEMP);

  // Set maximum
  Double_t bkgmax = stackplotsddirr->GetMaximum();
  Double_t overallmax = TMath::Max(datamax, bkgmax);

  stackplotsddirr->SetMaximum(1.24*overallmax);

  if(datamax==0)
  {
    std::cout << "NO DATA: " << std::endl;
    dataTemp->SetMaximum(20.0*overallmax);
    totalbkg->SetMaximum(20.0*overallmax);
    stackplotsddirr->SetMaximum(1.15*overallmax);
  }

  stackplotsddirr->Draw("HIST");
  stackplotsddirr->GetYaxis()->SetTitle(strParameters[0]);
  stackplotsddirr->GetYaxis()->SetTitleOffset(1.1);
  stackplotsddirr->GetYaxis()->SetTitleFont(42);
  stackplotsddirr->GetYaxis()->SetTitleSize(0.055);
  stackplotsddirr->GetYaxis()->SetLabelFont(42);
  stackplotsddirr->GetYaxis()->SetLabelSize(0.05);
  stackplotsddirr->GetXaxis()->SetTitle(strParameters[1]);
  stackplotsddirr->GetXaxis()->SetTitleOffset(1.2);
  stackplotsddirr->GetXaxis()->SetTitleFont(42);
  stackplotsddirr->GetXaxis()->SetTitleSize(0.055);
  stackplotsddirr->GetXaxis()->SetLabelFont(42);
  stackplotsddirr->GetXaxis()->SetLabelSize(0.05);

  // Set minimum for all histograms (need all bins to show up in plots. 0 bkg bins show errors)
  Double_t totalbkgmin = totalbkg->GetMinimum();
  Double_t nonzerototalmin = 10000;//some large value (not very good but ok)
  for(int i = 1; i <= (int)totalbkg->GetNbinsX(); i++)
  {
    if(1==i) nonzerototalmin = totalbkg->GetBinContent(i);
    if(totalbkg->GetBinContent(i) < nonzerototalmin && totalbkg->GetBinContent(i)>0) nonzerototalmin = totalbkg->GetBinContent(i);//same threshold as the one used above
  }

  // For loop added from above for totalbkgerror
  for(Int_t bkgbin = 0; bkgbin <= (Int_t)totalbkg->GetNbinsX(); bkgbin++)
    {
      totalbkg->SetBinError(bkgbin, totalbkgerrorTEMP->GetBinContent(bkgbin));
      if(totalbkg->GetBinContent(bkgbin)==0)//want no empty bins. Replace all 0s by minimum
      {
        totalbkg->SetBinContent(bkgbin, 1.1*nonzerototalmin/20.0);//same as minimum in plot
	totalbkg->SetBinError(bkgbin, totalbkgerrorTEMP->GetBinContent(bkgbin)-(1.1*nonzerototalmin/20.0));
      }
      std::cout << "BKG ERROR: " << bkgbin << " " << totalbkg->GetBinContent(bkgbin) 
                << " " << totalbkgerrorTEMP->GetBinContent(bkgbin) << std::endl;
    }
  
  if(totalbkgmin!=0) totalbkg->SetMinimum(totalbkgmin/2.0);
  else totalbkg->SetMinimum(nonzerototalmin/20.0);//1.0e-3

  //==============Drawing starts==========================
  totalbkg->Draw("E2same");
  dataTemp->Draw("pesame");

  // About to draw title
  TLatex *tex = new TLatex();
  tex->SetTextColor(1);
  tex->SetTextAlign(10);
  tex->SetTextSize(0.05);
  tex->SetTextSize(0.040);
  tex->SetTextFont(62); //42
  tex->SetLineWidth(2);
  tex->SetNDC();

  tex->DrawLatex(0.13, yposition, cmslabel);
  //tex->DrawLatex(xposition, 0.915, "#sqrt{s} = 8 TeV, L_{int} = "+luminosity+" fb^{-1}");
  //tex->DrawLatex(xposition, 0.915, "#sqrt{s} = 8 TeV, #scale[0.75]{#int}#it{L} d#it{t} = "+luminosity+" fb^{-1}");
  tex->DrawLatex(0.46, 0.940, "#sqrt{s} = 8 TeV");
  tex->DrawLatex(0.775, 0.940, "L = "+luminosity+" fb^{-1}");

  // Smarter coordinates for TLegend: moved from above
  TLegend *ddirrlegend = new TLegend(0.55, 0.45, 0.78, 0.86, "", "brNDC");
  ddirrlegend->SetTextFont(42);
  ddirrlegend->SetFillColor(0);
  ddirrlegend->SetShadowColor(0);
  ddirrlegend->SetBorderSize(0);
  ddirrlegend->SetTextSize(0.045);

  //ddirrlegend->AddEntry(dataTemp, "Observed", "lep");
  ddirrlegend->AddEntry(dataTemp, "Data", "lep");
  //if(1==include_signal) ddirrlegend->AddEntry(signalTEMP, signalname, "f");

// better legend ordering

  ddirrlegend->AddEntry(totalbkg, "Bkg uncertainty", "f");//Post-Approval
  ddirrlegend->AddEntry(ddbkgTEMP, "Misidentified", "f");
  ddirrlegend->AddEntry(ddirrTempHistCol[4], "t#bar{t}", "f");
  ddirrlegend->AddEntry(ddirrTempHistCol[1], "t#bar{t}W", "f");
  ddirrlegend->AddEntry(ddirrTempHistCol[0], "t#bar{t}Z", "f");
  ddirrlegend->AddEntry(ddirrTempHistCol[3], "WZ", "f");
  ddirrlegend->AddEntry(ddirrTempHistCol[2], "ZZ", "f");

// Sanjay stacking order
/*
  ddirrlegend->AddEntry(totalbkg, "Bkg uncertainty", "f");//Post-Approval
  ddirrlegend->AddEntry(ddbkgTEMP, "Data-driven", "f");
  ddirrlegend->AddEntry(ddirrTempHistCol[4], "t#bar{t}", "f");
  ddirrlegend->AddEntry(ddirrTempHistCol[3], "WZ", "f");
  ddirrlegend->AddEntry(ddirrTempHistCol[2], "ZZ", "f");
  ddirrlegend->AddEntry(ddirrTempHistCol[1], "t#bar{t}W", "f");
  ddirrlegend->AddEntry(ddirrTempHistCol[0], "t#bar{t}Z", "f");
*/
  ddirrlegend->Draw();

  //=====================================End of generating all data-driven Bkg histograms==================================
}

// Convert plot name to descriptive title
TString ControlRegionAnalysis::converttoname(TString plotname)
{
  std::cout << "In ControlRegionAnalysis class converttoname method!" << std::endl;

  TString desc = "";
  if(plotname.Contains("L3")) desc += "3-leptons+";
  if(plotname.Contains("L4")) desc += "4-leptons+";

  if(plotname.Contains("DY0")) desc += "OSSF0+";
  if(plotname.Contains("DY1")) desc += "OSSF1+";
  if(plotname.Contains("DY2")) desc += "OSSF2+";
  
  if(plotname.Contains("ZVZL")) desc += "low-Z+";
  else if(plotname.Contains("ZVZH")) desc += "high-Z+";
  else if(plotname.Contains("ZV")) desc += "off-Z+";
  else if(plotname.Contains("DY1") || plotname.Contains("DY2")) desc += "on-Z+";

  if(plotname.Contains("Tau0")) desc += "no taus+";
  else if(plotname.Contains("Tau1")) desc += "1-tau+";
  
  if(plotname.Contains("b0")) desc += "no b-jets+";
  else if(plotname.Contains("b1")) desc += "at least 1 b-jet+";

  if(plotname.Contains("HTV")) desc += "high H_{T}";
  else desc += "low H_{T}";

  return desc;
}

void ControlRegionAnalysis::makeTestControlPlots(std::vector<std::string> dataHist,
                                                 std::vector<std::string> backgroundHist)
{
  std::cout << "In ControlRegionAnalysis class makeTestControlPlots method!" << std::endl;

  // Reset ROOT
  gROOT->Reset();

  // Set sumw2 on by default and detach histogram from file by default
  TH1::SetDefaultSumw2(kTRUE);
  TH1::AddDirectory(kFALSE);

  TFile *f = new TFile(storefile.c_str());

  // Converting luminosity string into an integer [fb]
  double lumi;
  std::stringstream ss;
  ss << luminosity;
  ss >> lumi;

  TCanvas *c1 = new TCanvas("c1", "c1", 800, 600);
  c1->SetBorderMode(0);
  c1->SetFillColor(kWhite);
  c1->cd();

  // CFO uncertainties
  //Double_t cmu = 0.00714*0.5*(1.0465+0.74);
  //Double_t cmu_err = 0.30;

  //Double_t ce = 0.00914*0.5*(0.6919+0.80);
  //Double_t ce_err = 0.30;

  //Double_t tau = 0.20*1.0768; // Low HT
  //Double_t tau = 0.05*0.96; // High HT
  //Double_t tau_err = 0.30;

  // Asymmetric Interanl Photon Conversion uncertainties
  //Double_t convmu = 0.00707;
  //Double_t convmu_err = 0.50;

  //Double_t conve = 0.0198;
  //Double_t conve_err = 0.5;

  // Data luminosity [pb]
  Double_t data_lumi = 1000*lumi;

  // Branching ratios
  Double_t WtoLNu = 0.3203;
  Double_t Wto2J = (1.0-0.3203);

  // Cross sections [pb]
  Double_t xsec_ttbar = 225.0;
  Double_t xsec_ttbar_fullyleptonic = pow(WtoLNu,2)*xsec_ttbar;
  Double_t xsec_ttbar_semileptonic = 2.0*WtoLNu*Wto2J*xsec_ttbar;
  Double_t xsec_wz = 32.3*(0.3257*0.10095)*1.20;
  Double_t xsec_zz = 0.92*17.7*(0.10095*0.10095)*1.08;
  Double_t xsec_ttw = 0.23;
  Double_t xsec_ttz = 0.208;

  // Number of generated events
  Double_t N_ttbar_fullyleptonic = 9.20104e+05+4.53536e+05+4.61283e+05+4.55184e+05+4.62597e+05+4.61627e+05+4.61345e+05+4.59597e+05+3.41874e+05;
  Double_t N_ttbar_semileptonic = 2.40481e+05+8.31427e+05+1.59079e+05+1.61002e+05+1.60029e+05+1.60359e+05+1.5889e+05+1.60191e+05+1.60265e+05+1.10807e+05;
  Double_t N_wz = 2.96073e+05+1.98916e+05+0.000000e+00;
  Double_t N_zz = 6.75305e+05+4.46457e+05+4.41662e+05+1.52382e+05;
  Double_t N_ttw = 2.17827e+05;
  Double_t N_ttz = 2.09677e+05;

  // Multilepton filtering efficiencies
  Double_t ttbar_filter_fullyleptonic = 0.3697;
  Double_t ttbar_filter_semileptonic = 0.064;
  Double_t wz_filter = 0.244;
  Double_t zz_filter = 0.358;

  // Scaling factors
  Double_t alpha_ttbar_fulllep = (ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic)*data_lumi/N_ttbar_fullyleptonic;
  Double_t alpha_ttbar_semilep = (ttbar_filter_semileptonic*xsec_ttbar_semileptonic)*data_lumi/N_ttbar_semileptonic;
  //Double_t alpha_ttbar = (ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic)*data_lumi/(ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic/(ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic)*N_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic*(ttbar_filter_fullyleptonic*xsec_ttbar_fullyleptonic+ttbar_filter_semileptonic*xsec_ttbar_semileptonic)*N_ttbar_semileptonic);
  Double_t alpha_wz = wz_filter*xsec_wz*data_lumi/N_wz;
  Double_t alpha_zz = zz_filter*xsec_zz*data_lumi/N_zz;
  Double_t alpha_ttw = xsec_ttw*data_lumi/N_ttw;
  Double_t alpha_ttz = xsec_ttz*data_lumi/N_ttz;

  // Errors
  Double_t max_error = 0.0;
  //Double_t total_error = 0.0;
  //Double_t total_bin_error = 0.0;

  // Collision data
  TH1D *h_data = new TH1D();
  TH1D *h_ttbar_fulllep = new TH1D();
  TH1D *h_ttbar_semilep = new TH1D();
  TH1D *h_ttbar = new TH1D();
  TH1D *h_wz = new TH1D();
  TH1D *h_zz = new TH1D();
  TH1D *h_ttw = new TH1D();
  TH1D *h_ttz = new TH1D();
  //TH1D *h_higgs = new TH1D();
  //TH1D *h_ddbackground = new TH1D();

  // For Data-driven contributions
  //TH1D *h_ddCFOMu = new TH1D();
  //TH1D *h_ddCFOEl = new TH1D();
  //TH1D *h_ddCONVMu = new TH1D();
  //TH1D *h_ddCONVEl = new TH1D();
  //TH1D *h_ddTAU = new TH1D();

  // For Data-driven systematics
  //TH1D *h_ddbackground_Err = new TH1D();

  // About to get histograms
  if (strParameters[2] == "2D") {
    // Collision data
    h_data = (TH1D *)((TH2D *)f->Get(dataHist[0].c_str()))->ProjectionX()->Rebin(5);

    // TTbar Fully Leptonic background
    h_ttbar_fulllep = (TH1D *)((TH2D *)f->Get(backgroundHist[0].c_str()))->ProjectionX()->Rebin(5);
    h_ttbar_fulllep->Scale(alpha_ttbar_fulllep);

    // TTbar Semi Leptonic background
    h_ttbar_semilep = (TH1D *)((TH2D *)f->Get(backgroundHist[1].c_str()))->ProjectionX()->Rebin(5);
    h_ttbar_semilep->Scale(alpha_ttbar_semilep);

    // WZ background
    h_wz = (TH1D *)((TH2D *)f->Get(backgroundHist[2].c_str()))->ProjectionX()->Rebin(5);
    h_wz->Scale(alpha_wz);

    // ZZ background
    h_zz = (TH1D *)((TH2D *)f->Get(backgroundHist[3].c_str()))->ProjectionX()->Rebin(5);
    h_zz->Scale(alpha_zz);

    // TTW background
    h_ttw = (TH1D *)((TH2D *)f->Get(backgroundHist[4].c_str()))->ProjectionX()->Rebin(5);
    h_ttw->Scale(alpha_ttw);

    // TTZ background
    h_ttz = (TH1D *)((TH2D *)f->Get(backgroundHist[5].c_str()))->ProjectionX()->Rebin(5);
    h_ttz->Scale(alpha_ttz);

    // Data-driven background
    //h_ddbackground = (TH1D *)((TH2D *)f->Get(backgroundHist[5].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven ddCFOMu background
    //h_ddCFOMu = (TH1D *)((TH2D *)f->Get(backgroundHist[6].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven h_ddCFOEl background
    //h_ddCFOEl = (TH1D *)((TH2D *)f->Get(backgroundHist[7].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven ddCONVMu1D background
    //h_ddCONVMu = (TH1D *)((TH2D *)f->Get(backgroundHist[8].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven h_ddCONVEl background
    //h_ddCONVEl = (TH1D *)((TH2D *)f->Get(backgroundHist[9].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven h_ddCONVEl background
    //h_ddTAU = (TH1D *)((TH2D *)f->Get(backgroundHist[10].c_str()))->ProjectionX()->Rebin(5);

    // Higgs background
    //h_higgs = (TH1D *)((TH2D *)f->Get(backgroundHist[11].c_str()))->ProjectionX()->Rebin(5);

    // Data-driven h_ddbackground_Err
    //h_ddbackground_Err = (TH1D *)h_data->Clone();
    //h_ddbackground_Err->Reset("ICES");
  }

  // TTbar background (i.e. fully leptonic + semileptonic)
  h_ttbar =  (TH1D *)h_ttbar_fulllep->Clone();
  h_ttbar->Add(h_ttbar_semilep);


  // Collision data
  h_data->SetMarkerStyle(8);
  h_data->SetFillColor(kBlack);
  h_data->SetLineColor(kBlack);

  // TTbar background
  h_ttbar->SetFillColor(kMagenta-7);
  h_ttbar->SetLineColor(kBlack);

  // WZ background
  h_wz->SetFillColor(kSpring+7);
  h_wz->SetLineColor(kBlack);

  // ZZ background
  h_zz->SetFillColor(kSpring+4);
  h_zz->SetLineColor(kBlack);

  // TTW background
  h_ttw->SetFillColor(kCyan-3);
  h_ttw->SetLineColor(kBlack);

  // TTZ background
  h_ttz->SetFillColor(kCyan);
  h_ttz->SetLineColor(kBlack);

  // Higgs background
  //h_higgs->SetFillColor(kBlue);
  //h_higgs->SetLineColor(kBlack);

  // Data-driven background
  //h_ddbackground->SetFillColor(kYellow);
  //h_ddbackground->SetLineColor(kBlack);
  //h_ddCFOMu->SetFillColor(kYellow);
  //h_ddCFOMu->SetLineColor(kBlack);
  //h_ddCONVMu->SetFillColor(92);
  //h_ddCONVMu->SetLineColor(kBlack);

  // Systematic errors
  TH1D *h_systematic_error = new TH1D("", "", 16, 0, 400);
  h_systematic_error->SetFillColor(kBlue-8);
  h_systematic_error->SetFillStyle(3344);
  h_systematic_error->SetMarkerStyle(0.0);
  
  h_systematic_error->Add(h_zz);
  h_systematic_error->Add(h_wz);
  h_systematic_error->Add(h_ttz);
  h_systematic_error->Add(h_ttw);
  h_systematic_error->Add(h_ttbar);
  //h_systematic_error->Add(h_higgs);
  //h_systematic_error->Add(h_ddCFOMu);
  //h_systematic_error->Add(h_ddCFOEl);
  //h_systematic_error->Add(h_ddTAU);
  //h_systematic_error->Add(h_ddCONVMu);
  //h_systematic_error->Add(h_ddCONVEl);

  // Setting systematic uncertainty
/*
  for (Int_t i = 1; i < h_systematic_error->GetNbinsX(); i++) {

    h_ddbackground_Err->SetBinContent(i,cmu_err*h_ddbackground->GetBinContent(i));

    total_bin_error = sqrt(pow(h_ddbackground_Err->GetBinContent(i),2)+h_ddbackground->GetBinContent(i)+pow(0.50*h_ttbar->GetBinContent(i),2)+pow(0.06*h_ttbar->GetBinContent(i),2)+pow(alpha_ttbar*sqrt(h_ttbar->GetBinContent(i)),2)+pow(0.06*h_wz->GetBinContent(i),2)+pow(alpha_wz*sqrt(h_wz->GetBinContent(i)),2)+pow(0.12*h_zz->GetBinContent(i),2)+pow(alpha_zz*sqrt(h_zz->GetBinContent(i)),2)+pow(0.5*h_ttw->GetBinContent(i),2)+pow(alpha_ttw*sqrt(h_ttw->GetBinError(i)),2)+pow(0.5*h_ttz->GetBinContent(i),2)+pow(alpha_ttz*sqrt(h_ttz->GetBinContent(i)),2));

    h_systematic_error->SetBinError(i, total_bin_error);

    max_error = TMath::Max(max_error, total_bin_error);
  }

  // Calculating total error
  for (Int_t i = 1; i < h_systematic_error->GetNbinsX(); i++) {
      total_error += h_systematic_error->GetBinError(i);
  }
*/
  // About to get stack
  THStack *hs_background = new THStack("hs_background", "");

  //hs_background->Add(h_ddCFOMu);
  //hs_background->Add(h_ddCFOEl);
  //hs_background->Add(h_ddTAU);
  hs_background->Add(h_zz);
  hs_background->Add(h_wz);
  hs_background->Add(h_ttbar);
  hs_background->Add(h_ttz);
  hs_background->Add(h_ttw);
  //hs_background->Add(h_higgs);
  //hs_background->Add(h_ddCONVMu);
  //hs_background->Add(h_ddCONVEl);

  // Setting maximum y-axis range
  Double_t yaxis_min = 0.0;
  Double_t yaxis_max = 1.0;

  yaxis_max = TMath::Max(h_data->GetMaximum(), hs_background->GetMaximum()+max_error);

  hs_background->SetMaximum(1.25*yaxis_max);
  hs_background->SetMinimum(yaxis_min);
  hs_background->Draw("HIST");

  hs_background->GetYaxis()->SetTitle(strParameters[0]);
  hs_background->GetYaxis()->SetTitleOffset(1.1);
  hs_background->GetYaxis()->SetTitleFont(42);
  hs_background->GetYaxis()->SetTitleSize(0.055);
  hs_background->GetYaxis()->SetLabelFont(42);
  hs_background->GetYaxis()->SetLabelSize(0.040);
  hs_background->GetXaxis()->SetTitle(strParameters[1]);
  hs_background->GetXaxis()->SetTitleOffset(1.2);
  hs_background->GetXaxis()->SetTitleFont(42);
  hs_background->GetXaxis()->SetTitleSize(0.055);
  hs_background->GetXaxis()->SetLabelFont(42);
  hs_background->GetXaxis()->SetLabelSize(0.040);

  // Print out statements
  std::cout << "Total Observation: "<< h_data->Integral(0, h_data->GetNbinsX()) << std::endl;
  //std::cout << "Total Background: " << h_systematic_error->Integral(0, h_systematic_error->GetNbinsX()) << std::endl;
  //std::cout << "Total Background Uncertainty: " << total_error << std::endl;
  //std::cout << "Total Data-driven: "<< h_ddbackground->Integral(0, h_ddbackground->GetNbinsX()) << std::endl;
  std::cout << "Total TTbar MC: "<< h_ttbar->Integral(0, h_ttbar->GetNbinsX()) << std::endl;
  std::cout << "Total WZ MC: "<< h_wz->Integral(0, h_wz->GetNbinsX()) << std::endl;
  std::cout << "Total ZZ MC: "<< h_zz->Integral(0, h_zz->GetNbinsX()) << std::endl;
  std::cout << "Total TTW MC: "<< h_ttw->Integral(0, h_ttw->GetNbinsX()) << std::endl;
  std::cout << "Total TTZ MC: "<< h_ttz->Integral(0, h_ttz->GetNbinsX()) << std::endl;

  // About to draw plots
  hs_background->Draw("HIST");
  h_data->Draw("pesame"); // "pex0same"
  //h_systematic_error->Draw("E2same");

  // About to draw title
  TLatex *tex = new TLatex();
  tex->SetTextColor(1);
  tex->SetTextAlign(10);
  tex->SetTextSize(0.05);
  tex->SetTextSize(0.040);
  tex->SetTextFont(62); //42
  tex->SetLineWidth(2);
  tex->SetNDC();

  tex->DrawLatex(0.13, yposition, cmslabel);
  tex->DrawLatex(xposition, 0.915, "#sqrt{s} = 8 TeV, #scale[0.75]{#int}#it{L} d#it{t} = "+luminosity+" fb^{-1}");

  // About to draw legend  
  TLegend *leg = new TLegend(0.65, 0.45, 0.88, 0.87, "", "brNDC");
  leg->SetTextFont(42);
  leg->SetFillColor(0);
  leg->SetShadowColor(0);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.04);

  leg->AddEntry(h_data, "Data", "lep");
  //leg->AddEntry(h_systematic_error, "Bkg uncertainty", "f");
  //leg->AddEntry(h_ddCONVMu, "#font[132]{#it{l}}#font[132]{#it{l}}+#gamma", "f");
  //leg->AddEntry(h_ddCFOMu, "#font[132]{#it{l}}#font[132]{#it{l}}+jet", "f");
  leg->AddEntry(h_ttbar, "t#bar{t}", "f");
  leg->AddEntry(h_wz, "WZ", "f");
  leg->AddEntry(h_zz, "ZZ", "f");
  leg->AddEntry(h_ttw, "t#bar{t}W", "f");
  leg->AddEntry(h_ttz, "t#bar{t}Z", "f");
  //leg->AddEntry(h_higgs, "Higgs", "f");

  leg->Draw();
}

#endif
