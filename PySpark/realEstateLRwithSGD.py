# Load default Apache Spark libraries
from pyspark import SparkConf, SparkContext, SQLContext, Row

# Instantiate spark context
cf = SparkConf().setAppName("appName").setMaster("local[*]")
sc = SparkContext(conf=cf)
sqlContext = SQLContext(sc)

# Suppress spark log messages
log4j = sc._jvm.org.apache.log4j
log4j.LogManager.getLogger("org").setLevel(log4j.Level.OFF)
log4j.LogManager.getLogger("akka").setLevel(log4j.Level.OFF)
log4j.LogManager.getRootLogger().setLevel(log4j.Level.ERROR)

# Formatting a CSV into a DataFrame
rdd = sc.textFile("Sacramentorealestatetransactions.csv")
print "1st 5 elements of the RDD of csv\n {}".format(rdd.take(5))

# Separate lines using split on commas (broken into Spark's RDD tuple format)
rdd = rdd.map(lambda line: line.split(","))
print "1st 2 elements from the now comma split string RDD\n {}".format(rdd.take(2))

# Print header from the RDD by passing a filter to keep only RDD members not equal to the header
header = rdd.first()
rdd = rdd.filter(lambda line: line != header)

print "1st 2 elements with header removed\n {}".format(rdd.take(2))

# Use Row to map every line from the RDD to a Row for the soon to be DataFrame. Pipe result with .toDF() to produce Spark DataFrame
df = rdd.map(lambda line: Row(street=line[0],city=line[1],zip=line[2],beds=line[4],baths=line[5],sqft=line[6],price=line[9])).toDF()

# DataFrame responds to some RDD syntax such as take() method, though not especially readable, instead use show()
print "1st 5 elements of the DataFrame using take\n {}".format(df.take(5))
print "1st 5 elements of the DataFrame using show \n",df.show(5)

# Pandas (data analysis tool). PySpark has the option to convert to Pandas dataframes and vis versa
print "Convert pySpark DataFrame to Pandas dataframe\n",df.toPandas().head()

# Pandas dataframe operations. Select from dataframe homes in the zip code 95815 only
favorite_zip = df[df.zip == 95815]
print "Show the first 5 elements of the pandas dataframe\n",favorite_zip.show(5)

# Can choose a subset of columns from the dataframe by calling .select() on the dataframe
df.select('city','beds').show(10)

# Count number of houses with different number of bedrooms (some homes have 0 beds). 
df.groupBy('beds').count().show()

# Retrieve summary statistics of columns using describe(), which returns a dataframe so use .show() to see summary. 
df.describe(['baths','beds','price','sqft']).show()


# Regression with spark MLlib (simple linear regression example below)
# Goal: Predict home price from the number of baths, beds, and square feet
import pyspark.mllib
import pyspark.mllib.regression
from pyspark.mllib.regression import LabeledPoint
from pyspark.sql.functions import *

# Begin by creating a DataFrame that has only the subset of features you're interested in.
df = df.select('price','baths','beds','sqft')

# Remove suspicious 0 values for any of the features we want to use for prediction
df = df[df.baths > 0]
df = df[df.beds > 0]
df = df[df.sqft > 0]
df.describe(['baths','beds','price','sqft']).show()

# Labeled Points and Scaling data
# Note: MLlib requires the label and features to be expressed with LabeledPoints format, i.e. tuple of the response value (target) and a vector of predictors (features)
tmpDF = df.map(lambda line: LabeledPoint(line[0], [line[1:]]))
print "1st 5 LabeledPoints elements of the DataFrame\n {}".format(tmpDF.take(5))

# We now have the data format required by MLlib. 
# we will use Stochastic Gradient Descent (SGD). 
# The scale of the square footage of these houses is quite large in comparison to the number of bedrooms & bathrooms, 
# we will need to scale the data with spark's StandardScalaer
from pyspark.mllib.util import MLUtils
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.feature import StandardScaler

# The features will need to be scaled
features = df.map(lambda row: row[1:])
print "1st 5 features\n {}".format(features.take(5))

standardizer = StandardScaler()
model = standardizer.fit(features)
features_transform = model.transform(features)

print "1st 5 feature scale tranformed elements\n {}".format(features_transform.take(5))

# Putting labels together with features (combine labels and features as LabeledPoints)
# Note labels (prices) are in DataFrame format while scaled features are in the new RDD format just created.
# In order to scale the data, we'll have to use an RDD
lab = df.map(lambda row: row[0])
print "1st 5 Row elements of the DataFrame\n {}".format(lab.take(5))

# Now these 2 RDDs can be put together with the zip utility
transformData = lab.zip(features_transform)
print "1st 5 elements of the RDD (label, features)\n {}".format(transformData.take(5))

# Convert back to the LabeledPoint format (i.e. structure) before using MLlib
transformData = transformData.map(lambda row: LabeledPoint(row[0],row[1]))
print "1st 5 elements of RDD of Labeled points\n {}".format(transformData.take(5))

# MLlib training and testing parsing (can add an a valdiation set by incluing its % of data assigned for it)
trainingData, testingData = transformData.randomSplit([.8,.2],seed=1234)

# Import linear regression wth stochastic gradient descent and build a model. 
# Number of iterations is specified along with the step size and data set.
from pyspark.mllib.regression import LinearRegressionWithSGD
maxIter = 1000
stepSize = 0.2
linearModel = LinearRegressionWithSGD.train(trainingData, maxIter, stepSize)

# Extract features such as coefficients and intercepts from the model
print "linear model weights\n {}".format(linearModel.weights)

# Show the first 10 points 
print "1st 10 points of testing data\n {}".format(testingData.take(10))

# Make a prediction on one of the point using our model
print "Model predction for a give set of input features\n {}".format(linearModel.predict([1.49297445326,3.52055958053,1.73535287287]))

# Evaluating the model with Metrics (import RegressionMetrics)
# First let's look at the R-squared by examining the training data
from pyspark.mllib.evaluation import RegressionMetrics

# Need RDD that's a tuple of prediction from the SGDLR model and the original home values. Wrap predictions in 'float()' to avoid conflict between float types ocurred between standard python float and numpy float 32, when RegressionMetrics is called
obsPredRDDin = trainingData.map(lambda row: (float(linearModel.predict(row.features)),row.label))

# Calling RegressionMetrics on this RDD builds a variable that contains a number of metrics
metrics = RegressionMetrics(obsPredRDDin)

# Retrieve R-squared value for training data
print "Training R-squared value\n {}".format(metrics.r2)

# Retrieve RMSE value for training data
print "Training RMSE value\n {}".format(metrics.rootMeanSquaredError)

# Do the same RDD mapping and RegressionMetrics for the test data set.
obsPredRDDout = testingData.map(lambda row: (float(linearModel.predict(row.features)), row.label))
metrics = RegressionMetrics(obsPredRDDout)

# Call the R-squared value on the test data
print "Test R-squared value\n {}".format(metrics.r2)

# Call the RMSE on the test data
print "Test RMSE value\n {}".format(metrics.rootMeanSquaredError)
