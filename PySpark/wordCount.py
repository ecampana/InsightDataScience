# Load common python libraries
import re

# Load default Apache Spark libraries
from pyspark import SparkConf, SparkContext, SQLContext, Row

# Instantiate spark context
cf = SparkConf().setAppName("appName").setMaster("local[*]")
sc = SparkContext(conf=cf)
sqlContext = SQLContext(sc)

# Suppress spark log messages
log4j = sc._jvm.org.apache.log4j
log4j.LogManager.getLogger("org").setLevel(log4j.Level.OFF)
log4j.LogManager.getLogger("akka").setLevel(log4j.Level.OFF)
log4j.LogManager.getRootLogger().setLevel(log4j.Level.ERROR)


# Filename
fileName = "shakespeare.txt"


# Define the function removePunctuation that converts all text to lower case, removes any
# punctuation, and removes leading and trailing spaces.
def removePunctuation(text):
    return re.compile(r'[^a-z0-9 ]').sub("",text.lower()).strip()


# First, define a function for word counting. You should reuse the techniques that have been covered
# in earlier parts of this lab. This function should take in an RDD that is a list of words like wordsRDD
# and return a pair RDD that has all of the words and their associated counts.
def wordCount(wordListRDD):
    return wordListRDD.map(lambda str: (str,1)).reduceByKey(lambda wp1,wp2: wp1+wp2)


# Loading text file and converting into an RDD
shakespeareRDD = sc.textFile(fileName, 8).map(removePunctuation)


# Print line number and line content
print '\n'.join(shakespeareRDD
                .zipWithIndex()  # to (line, lineNum)
                .map(lambda (l, num): '{0}: {1}'.format(num, l))  # to 'lineNum: line'
                .take(15))


# Split each line by its spaces
shakespeareWordsRDD = shakespeareRDD.flatMap(lambda str: str.split(' '))
print shakespeareWordsRDD.first()
print shakespeareWordsRDD.count()

# Filter out empty lines
shakeWordsRDD = shakespeareWordsRDD.filter(lambda wd: wd != '')
print shakeWordsRDD.first()
print shakeWordsRDD.count()

# Obtain the fifteen most common words and their counts.
top15WordsAndCounts = wordCount(shakeWordsRDD).takeOrdered(15, key = lambda vl: -vl[1])

# Print out the results
print '\n'.join(map(lambda (w, c): '{0}: {1}'.format(w, c), top15WordsAndCounts))

