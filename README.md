Insight Data Science Fellowship program
==============

About the repository
--------------

**Rutgers University** 

*Creator*: 
Emmanuel Contreras-Campana, PhD

- July 2016

This repository contains example programs in c++ (using ROOT6), pyspark (Apache Spark), 
and R programming. The directory ControlRegionAnalysis has selected code that 
was used to complete thesis research, the PySpark directory contains a program 
for performing a simple linear regression analysis on California home prices, while 
the directory RProgramming contains knitr code used to complete the Statistical 
Inference course given by Johns Hopkins University on Coursera. Lastly, the Projects
directory contains a pdf file of an analysis done for the Regression Models course 
also given by Johns Hopkins University on Coursera.
